package com.young;

import com.young.enums.CardEnum;
import com.young.factory.CardRiskServiceFactory;
import com.young.pojo.AgriculturalCard;
import com.young.pojo.BuilderCard;
import com.young.pojo.Card;
import com.young.pojo.PostalCard;
import com.young.request.CardRiskRequest;
import com.young.response.CardRiskResponse;
import com.young.service.CardRiskService;
import com.young.service.impl.AgriculturalCardRiskService;
import com.young.service.impl.EasyCardRiskService;
import com.young.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@Slf4j
public class StartegyApplicationTest {
    @Autowired
    private EasyCardRiskService easyCardRiskService;

//    @Resource
//    private CardRiskServiceFactory cardRiskServiceFactory;

    @Test
    public void test1(){
        Card card1=new AgriculturalCard();
        card1.setCardNo("123456");
        card1.setBalance(100);
        card1.setBankCode(CardEnum.AGRICULTURAL.name());
        card1.setBankName("中国农业银行");
        card1.setRegisterTime(System.currentTimeMillis());

        Card card2=new BuilderCard();
        card2.setCardNo("10086");
        card2.setBalance(100);
        card2.setBankCode(CardEnum.BUILDER.name());
        card2.setBankName("中国建设银行");
        card2.setRegisterTime(System.currentTimeMillis());

        Card card3=new PostalCard();
        card3.setCardNo("10000");
        card3.setBalance(200);
        card3.setBankCode(CardEnum.POSTAL.name());
        card3.setBankName("中国邮政银行");
        card3.setRegisterTime(System.currentTimeMillis());

        List<Card>cards=new ArrayList<>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        cards.stream().forEach(card->{
            CardRiskRequest cardRiskRequest=new CardRiskRequest();
            cardRiskRequest.setCardNo(card.getCardNo());
            cardRiskRequest.setBankCode(card.getBankCode());
            cardRiskRequest.setRegisterTime(card.getRegisterTime());
            cardRiskRequest.setUserName("cxy");
            CardRiskResponse cardRiskResponse = easyCardRiskService.consultRisk(cardRiskRequest);
            log.info("response:{}",cardRiskResponse);
        });

    }

    @Test
    public void test2(){
        System.out.println(AgriculturalCardRiskService.class.getName());
        Object bean = SpringContextUtil.getBean("agriculturalCardRiskService");
        System.out.println(bean==null);
        Card card1=new AgriculturalCard();
        card1.setCardNo("123456");
        card1.setBalance(100);
        card1.setBankCode(CardEnum.AGRICULTURAL.name());
        card1.setBankName("中国农业银行");
        card1.setRegisterTime(System.currentTimeMillis());

        Card card2=new BuilderCard();
        card2.setCardNo("10086");
        card2.setBalance(100);
        card2.setBankCode(CardEnum.BUILDER.name());
        card2.setBankName("中国建设银行");
        card2.setRegisterTime(System.currentTimeMillis());

        Card card3=new PostalCard();
        card3.setCardNo("10000");
        card3.setBalance(200);
        card3.setBankCode(CardEnum.POSTAL.name());
        card3.setBankName("中国邮政银行");
        card3.setRegisterTime(System.currentTimeMillis());

        List<Card>cards=new ArrayList<>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        cards.stream().forEach(card->{
            CardRiskRequest cardRiskRequest=new CardRiskRequest();
            cardRiskRequest.setCardNo(card.getCardNo());
            cardRiskRequest.setBankCode(card.getBankCode());
            cardRiskRequest.setRegisterTime(card.getRegisterTime());
            cardRiskRequest.setUserName("cxy");
//            CardRiskService cardRiskService = CardRiskServiceFactory.getInstance(card.getBankCode());
            CardRiskService cardRiskService = CardRiskServiceFactory.getInstance(card.getBankCode());
            CardRiskResponse cardRiskResponse = cardRiskService.consultRisk(cardRiskRequest);
            log.info("response:{}",cardRiskResponse);
        });

    }

    @Test
    public  void test3(){
        System.out.println(CardEnum.AGRICULTURAL.name());
        System.out.println(CardEnum.BUILDER.name());
        System.out.println(CardEnum.POSTAL.name());
    }
}
