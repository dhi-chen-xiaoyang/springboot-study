package com.young.request;

import lombok.Data;

@Data
public class CardRiskRequest {
    private String cardNo;
    private String bankCode;
    private Long registerTime;
    private String userName;
}
