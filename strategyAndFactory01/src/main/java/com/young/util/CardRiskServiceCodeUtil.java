package com.young.util;

import com.young.enums.CardEnum;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.util.Map;

public class CardRiskServiceCodeUtil {
    private static final String AGRICULTURAL="agriculturalCardRiskService";
    private static final String BUILDER="builderCardRiskService";
    private static final String POSTAL="postalCardRiskService";
    private static Map<String,String>serviceCodeMap=new ConcurrentReferenceHashMap<>();
    private static Map<String,String>codeServiceMap=new ConcurrentReferenceHashMap<>();
    static {
        serviceCodeMap.put(CardEnum.AGRICULTURAL.name(),AGRICULTURAL);
        serviceCodeMap.put(CardEnum.BUILDER.name(), BUILDER);
        serviceCodeMap.put(CardEnum.POSTAL.name(), POSTAL);

        codeServiceMap.put(AGRICULTURAL,CardEnum.AGRICULTURAL.name());
        codeServiceMap.put(BUILDER,CardEnum.BUILDER.name());
        codeServiceMap.put(POSTAL,CardEnum.POSTAL.name());
    }

    public static String getServiceName(String code){
        return serviceCodeMap.get(code);
    }

    public static String getCode(String serviceName){
        return codeServiceMap.get(serviceName);
    }
}
