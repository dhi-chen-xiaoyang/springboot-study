package com.young.response;

import lombok.Data;

@Data
public class CardRiskResponse {
    private Boolean result;
    private String unableReason;
    private String challengeType;
}
