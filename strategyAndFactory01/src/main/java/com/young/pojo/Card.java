package com.young.pojo;

import lombok.Data;

@Data
public class Card {
    private String cardNo;
    private String bankCode;
    private String bankName;
    private Long registerTime;
    private Integer balance;
}
