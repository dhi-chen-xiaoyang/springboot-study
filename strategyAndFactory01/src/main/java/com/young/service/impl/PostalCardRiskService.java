package com.young.service.impl;

import com.young.enums.CardEnum;
import com.young.factory.CardRiskServiceFactory;
import com.young.request.CardRiskRequest;
import com.young.response.CardRiskResponse;
import com.young.service.CardRiskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Slf4j
public class PostalCardRiskService implements CardRiskService {
    @Override
    public String getCardType() {
        return CardEnum.POSTAL.name();
    }

    @Override
    public CardRiskResponse consultRisk(CardRiskRequest cardRiskRequest) {
        log.info("邮政银行判断绑卡是否需要风控");
        CardRiskResponse cardRiskResponse=new CardRiskResponse();
        if (cardRiskRequest.getUserName().equals("cxy")){
            cardRiskResponse.setResult(true);
        }else{
            cardRiskResponse.setResult(false);
            cardRiskResponse.setChallengeType("OTP");
            cardRiskResponse.setUnableReason("用户信息需要风控");
        }
        return cardRiskResponse;
    }

//    @Override
//    public void afterPropertiesSet() throws Exception {
//        CardRiskServiceFactory.put(CardEnum.POSTAL.name(), this);
//    }
}
