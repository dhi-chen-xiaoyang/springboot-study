package com.young.service.impl;

import com.young.enums.CardEnum;
import com.young.request.CardRiskRequest;
import com.young.response.CardRiskResponse;
import com.young.service.CardRiskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Slf4j
public class EasyCardRiskService implements CardRiskService {
    @Override
    public String getCardType() {
        return "easyCardRiskService";
    }

    @Override
    public CardRiskResponse consultRisk(CardRiskRequest cardRiskRequest) {
        if (cardRiskRequest.getBankCode().equals(CardEnum.AGRICULTURAL.name())){
            return agriculturalRisk(cardRiskRequest);
        }else if (cardRiskRequest.getBankCode().equals(CardEnum.BUILDER.name())){
            return builderRisk(cardRiskRequest);
        } else if (cardRiskRequest.getBankCode().equals(CardEnum.POSTAL.name())) {
            return postalRisk(cardRiskRequest);
        }
        throw new UnsupportedOperationException();
    }

    private CardRiskResponse postalRisk(CardRiskRequest cardRiskRequest) {
        log.info("邮政银行判断绑卡是否需要风控");
        CardRiskResponse cardRiskResponse=new CardRiskResponse();
        if (cardRiskRequest.getUserName().equals("cxy")){
            cardRiskResponse.setResult(true);
        }else{
            cardRiskResponse.setResult(false);
            cardRiskResponse.setChallengeType("OTP");
            cardRiskResponse.setUnableReason("用户信息需要风控");
        }
        return cardRiskResponse;
    }

    private CardRiskResponse builderRisk(CardRiskRequest cardRiskRequest) {
        log.info("建设银行判断绑卡是否需要风控");
        CardRiskResponse cardRiskResponse=new CardRiskResponse();
        if (cardRiskRequest.getRegisterTime()%2==0){
            cardRiskResponse.setResult(true);
        }else{
            cardRiskResponse.setResult(false);
            cardRiskResponse.setChallengeType("OTP");
            cardRiskResponse.setUnableReason("注册时间需要验证");
        }
        return cardRiskResponse;
    }

    private CardRiskResponse agriculturalRisk(CardRiskRequest cardRiskRequest) {
        log.info("农业银行判断绑卡是否需要风控");
        CardRiskResponse cardRiskResponse=new CardRiskResponse();
        if (cardRiskRequest.getCardNo().startsWith("1000")){
            cardRiskResponse.setResult(true);
        }else{
            cardRiskResponse.setResult(false);
            cardRiskResponse.setChallengeType("OTP");
            cardRiskResponse.setUnableReason("不知名原因");
        }
        return cardRiskResponse;
    }
}
