package com.young.service.impl;

import com.young.enums.CardEnum;
import com.young.factory.CardRiskServiceFactory;
import com.young.request.CardRiskRequest;
import com.young.response.CardRiskResponse;
import com.young.service.CardRiskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Slf4j
public class BuilderCardRiskService implements CardRiskService  {
    @Override
    public String getCardType() {
        return CardEnum.BUILDER.name();
    }

    @Override
    public CardRiskResponse consultRisk(CardRiskRequest cardRiskRequest) {
        log.info("建设银行判断绑卡是否需要风控");
        CardRiskResponse cardRiskResponse=new CardRiskResponse();
        if (cardRiskRequest.getRegisterTime()%2==0){
            cardRiskResponse.setResult(true);
        }else{
            cardRiskResponse.setResult(false);
            cardRiskResponse.setChallengeType("OTP");
            cardRiskResponse.setUnableReason("注册时间需要验证");
        }
        return cardRiskResponse;
    }

//    @Override
//    public void afterPropertiesSet() throws Exception {
//        CardRiskServiceFactory.put(CardEnum.BUILDER.name(), this);
//    }
}
