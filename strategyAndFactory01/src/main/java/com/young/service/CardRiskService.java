package com.young.service;

import com.young.request.CardRiskRequest;
import com.young.response.CardRiskResponse;

public interface CardRiskService {
    String getCardType();
    default CardRiskResponse consultRisk(CardRiskRequest cardRiskRequest){
        throw new UnsupportedOperationException();
    }
}
