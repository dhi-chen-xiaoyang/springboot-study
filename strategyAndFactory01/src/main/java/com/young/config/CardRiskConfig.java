package com.young.config;

import com.young.StrategyApplication;
import com.young.factory.CardRiskServiceFactory;
import com.young.service.CardRiskService;
import com.young.util.CardRiskServiceCodeUtil;
import com.young.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Configuration
@Slf4j
public class CardRiskConfig {
    @Value("${card.risks}")
    private String []cardRisks;


    @PostConstruct
    public void init(){
        Arrays.stream(cardRisks).forEach(riskCode->{
            String serviceName= CardRiskServiceCodeUtil.getServiceName(riskCode);
            System.out.println("code:"+riskCode+",serviceName:"+serviceName);
            CardRiskService service =(CardRiskService) SpringContextUtil.getBean(serviceName);
            System.out.println("service:"+service);
            CardRiskServiceFactory.put(riskCode,service);
        });
    }
}
