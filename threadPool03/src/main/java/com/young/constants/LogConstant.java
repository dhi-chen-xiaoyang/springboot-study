package com.young.constants;

public class LogConstant {
    public final static String INFO="info";
    public final static String DEBUG="debug";
    public final static String ERROR="error";
    public final static String WARN="warn";
}
