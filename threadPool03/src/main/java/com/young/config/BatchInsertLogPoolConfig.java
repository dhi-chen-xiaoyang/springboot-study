package com.young.config;

import com.young.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sound.midi.MidiSystem;
import java.util.concurrent.*;

@Configuration
public class BatchInsertLogPoolConfig {
    //日志队列的最大容量
    private int MAX_QUEUE_SIZE=100;

    //线程睡眠时间，具体时间需要结合项目实际情况，单位毫秒
    private int SLEEP_TIME=500;

    //创建一个单线程的线程池
    @Bean
    @Qualifier(value = "batchInsertLogPool")
    public ThreadPoolExecutor batchInsertLogPool(){
//        return new ThreadPoolExecutor(1,0,500,TimeUnit.MILLISECONDS,
//                new LinkedBlockingDeque<>(Runtime.getRuntime().availableProcessors())
//            );
        return new ThreadPoolExecutor(2,2,60,TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(Runtime.getRuntime().availableProcessors()),
                Executors.defaultThreadFactory(),new ThreadPoolExecutor.CallerRunsPolicy());
    }
}
