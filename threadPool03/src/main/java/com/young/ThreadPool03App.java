package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreadPool03App {
    public static void main(String[] args) {
        SpringApplication.run(ThreadPool03App.class,args);
    }
}
