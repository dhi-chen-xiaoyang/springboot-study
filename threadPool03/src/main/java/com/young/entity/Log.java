package com.young.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName(value = "m_log")
public class Log implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String msg;
    private String url;
    private String type;
    private LocalDateTime createTime;
}
