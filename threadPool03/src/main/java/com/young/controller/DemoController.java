package com.young.controller;

import com.young.constants.LogConstant;
import com.young.entity.Log;
import com.young.logpool.BatchInsertLogThreadManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;


@RestController
@RequestMapping("/log")
public class DemoController {
    @Resource
    private BatchInsertLogThreadManager batchInsertLogThreadManager;
    @PostMapping
    public String testLog(HttpServletRequest request)  {
        Log log=new Log();
        log.setMsg("hello world"+System.currentTimeMillis());
        log.setCreateTime(LocalDateTime.now());
        log.setUrl(request.getRequestURL().toString());
        log.setType(LogConstant.DEBUG);
        try {
            batchInsertLogThreadManager.addLog(log);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }
//    @Autowired
//    WebApplicationContext applicationContext;
//    @GetMapping("/url")
//    public List<String> getUrl(){
//        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
//        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
//        List<String>urlList=new ArrayList<>();
//        for (RequestMappingInfo info:map.keySet()){
//            //获取url的set集合，一个方法可能对应多个url
//            Set<String> patterns = info.getPatternsCondition().getPatterns();
//            for (String url : patterns) {
//                urlList.add(url);
//            }
//        }
//        return urlList;
//    }
//    @GetMapping("/url1")
//    public String getUrl1(HttpServletRequest request){
//        StringBuffer url = request.getRequestURL();
//        System.out.println(url.toString());
//        String contextPath = request.getContextPath();
//        System.out.println(contextPath);
//        return url.toString();
//    }
}
