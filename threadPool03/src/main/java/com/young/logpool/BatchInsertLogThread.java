package com.young.logpool;

import com.young.entity.Log;
import com.young.service.LogService;

import java.util.List;

//用于批量插入日志的线程
public class BatchInsertLogThread implements Runnable{
    private List<Log>logList;
    private LogService logService;
    public BatchInsertLogThread(List<Log>logList,LogService logService){
        this.logList=logList;
        this.logService=logService;
        System.out.println("new BatchInsertLogThread==========="+logList.size());
    }
    @Override
    public void run() {
        System.out.println("批量插入======================");
        this.logService.batchInsert(logList);
    }
}
