package com.young.service;

import com.young.entity.Log;
import com.young.mapper.LogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogService {
    @Autowired
    private LogMapper logMapper;
    public void batchInsert(List<Log>list){
        logMapper.batchInsert(list);
    }
}
