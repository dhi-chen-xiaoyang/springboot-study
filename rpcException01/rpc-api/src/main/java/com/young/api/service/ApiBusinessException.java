package com.young.api.service;

import com.young.common.enums.ResultCode;

import java.io.Serializable;

public class ApiBusinessException extends RuntimeException{
    private ResultCode resultCode;
    public ApiBusinessException(){
    }
    public ApiBusinessException(ResultCode resultCode){
        super(resultCode.getMsg());
        this.resultCode=resultCode;
    }
    public ResultCode getResultCode(){
        return this.resultCode;
    }
}
