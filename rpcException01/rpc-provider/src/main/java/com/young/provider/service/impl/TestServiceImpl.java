package com.young.provider.service.impl;

import com.young.api.service.ApiBusinessException;
import com.young.api.service.TestService;
import com.young.common.enums.ResultCode;
import com.young.common.err.BusinessException;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.filter.ExceptionFilter;
import org.apache.dubbo.rpc.service.GenericException;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.stereotype.Service;

@Service
@DubboService
public class TestServiceImpl implements TestService {
    @Override
    public String hello1() {
        //抛出rpc-common中的异常
        throw new BusinessException(ResultCode.ERROR);
    }

    @Override
    public String hello2() {
        System.out.println("访问到hello2================");
        //抛出rpc-api中的异常
        throw new ApiBusinessException(ResultCode.ERROR);
    }

    @Override
    public String hello3(){
        return "hello world";
    }

}
