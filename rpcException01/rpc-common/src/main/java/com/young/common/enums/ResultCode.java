package com.young.common.enums;


import java.io.Serializable;

public enum ResultCode{
    SUCCESS(200,"操作成功"),
    FAIL(400,"操作失败"),
    ERROR(500,"服务器错误");
    private Integer code;
    private String msg;
    private ResultCode(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }
    public Integer getCode(){
        return this.code;
    }
    public String getMsg(){
        return this.msg;
    }
}
