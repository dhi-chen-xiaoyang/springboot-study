package com.young.common.vo;

import com.young.common.enums.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultVO <T> implements Serializable {
    private Integer code;
    private String msg;
    private T data;
    public ResultVO(ResultCode resultCode){
        this.code=resultCode.getCode();
        this.msg=resultCode.getMsg();
    }
    public ResultVO(ResultCode resultCode,T data){
        this.code=resultCode.getCode();
        this.msg=resultCode.getMsg();
        this.data=data;
    }
    public ResultVO(Integer code,String msg){
        this.code=code;
        this.msg=msg;
        this.data=null;
    }
}
