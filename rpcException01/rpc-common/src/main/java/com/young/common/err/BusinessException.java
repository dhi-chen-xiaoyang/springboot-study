package com.young.common.err;

import com.young.common.enums.ResultCode;

public class BusinessException extends RuntimeException{
    private ResultCode resultCode;
    public BusinessException(){}
    public BusinessException(ResultCode resultCode){
        super(resultCode.getMsg());
        this.resultCode=resultCode;
    }
    public ResultCode getResultCode(){
        return this.resultCode;
    }
}
