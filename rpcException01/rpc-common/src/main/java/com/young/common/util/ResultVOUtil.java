package com.young.common.util;

import com.young.common.enums.ResultCode;
import com.young.common.vo.ResultVO;

public class ResultVOUtil<T> {
    public static <T> ResultVO<T> success(){
        return new ResultVO<T>(ResultCode.SUCCESS);
    }
    public static <T> ResultVO<T> success(T data){
        return new ResultVO<T>(ResultCode.SUCCESS,data);
    }
    public static <T> ResultVO<T> fail(){
        return new ResultVO<T>(ResultCode.FAIL);
    }
    public static <T> ResultVO<T> fail(ResultCode resultCode){
        return new ResultVO<T>(resultCode);
    }
    public static <T> ResultVO<T> error(){
        return new ResultVO<T>(ResultCode.ERROR);
    }
}
