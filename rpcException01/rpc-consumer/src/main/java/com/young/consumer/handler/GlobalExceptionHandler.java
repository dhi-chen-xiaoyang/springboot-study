package com.young.consumer.handler;

import com.young.api.service.ApiBusinessException;
import com.young.common.enums.ResultCode;
import com.young.common.err.BusinessException;
import com.young.common.util.ResultVOUtil;
import com.young.common.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.filter.ExceptionFilter;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(value = BusinessException.class)
    public ResultVO handlerBusiness(BusinessException e){
        log.info("捕获businessException===============");
        return ResultVOUtil.fail(e.getResultCode());
    }

    @ExceptionHandler(value = ApiBusinessException.class)
    public ResultVO handlerApiBusinessException(ApiBusinessException e){
        log.info("捕获apiBusinessException===============");
        return ResultVOUtil.fail(e.getResultCode());
    }

    @ExceptionHandler(value = Exception.class)
    public ResultVO handlerException(Exception e){
        e.printStackTrace();
        log.info("捕获exception===============");
        return new ResultVO(ResultCode.FAIL.getCode(),e.getMessage());
    }
}
