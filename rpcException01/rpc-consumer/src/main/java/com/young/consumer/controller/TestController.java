package com.young.consumer.controller;

import com.young.api.service.TestService;
import com.young.common.util.ResultVOUtil;
import com.young.common.vo.ResultVO;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @DubboReference
    private TestService testService;

    @GetMapping("/hello1")
    public ResultVO hello1(){
        String s = testService.hello1();
        return ResultVOUtil.success(s);
    }

    @GetMapping("/hello2")
    public ResultVO hello2(){
        String s = testService.hello2();
        return ResultVOUtil.success(s);
    }

    @GetMapping("/hello3")
    public ResultVO hello3(){
        String s = testService.hello3();
        return ResultVOUtil.success(s);
    }
}
