package com.young;

import com.young.request.ChangePasswordRequest;
import com.young.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;

@SpringBootTest
public class SpringEvent01AppTest {
    @Autowired
    private UserService userService;

    @Test
    public void testUserLogin(){
        userService.login("cxy","123456");
    }

    @Test
    public void testUserChangePassword(){
        ChangePasswordRequest changePasswordRequest=new ChangePasswordRequest();
        changePasswordRequest.setUserId(1);
        changePasswordRequest.setOldPassword("123456");
        changePasswordRequest.setNewPassword("1234567");
        userService.changePassword(changePasswordRequest);
    }
}
