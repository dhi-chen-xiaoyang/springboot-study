package com.young.request;

import lombok.Data;

import java.io.Serializable;
@Data
public class ChangePasswordRequest implements Serializable {
    private Integer userId;
    private String oldPassword;
    private String newPassword;
}
