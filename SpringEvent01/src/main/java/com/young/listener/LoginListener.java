package com.young.listener;

import com.young.event.LoginEvent;
import lombok.extern.slf4j.Slf4j;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class LoginListener {
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void loginTel(LoginEvent loginEvent){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("接收到登录消息+发送短信,telephone={},goodsName={},=== {}",loginEvent.getTelePhone(),
                loginEvent.getGoodsName(), LocalDateTime.now());
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void loginEmail(LoginEvent loginEvent){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("接收到登录消息+发送邮箱,email={},goodsName={},=== {}",loginEvent.getTelePhone(),
                loginEvent.getGoodsName(), LocalDateTime.now());
    }
}
