package com.young.listener;

import com.young.event.UserChangePasswordMessageEvent;
import com.young.event.UserLoginMessageEvent;
import com.young.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageEventListener {
    @EventListener({UserLoginMessageEvent.class})
    public void userLoginMessageEventListener(UserLoginMessageEvent event){
        User user = event.getUser();
        log.info("发送短信到{}，提示用户{}已登录",user.getPhone(),user.getUsername());
    }

    @EventListener({UserChangePasswordMessageEvent.class})
    public void userChangePasswordMessageEventListener(UserChangePasswordMessageEvent event){
        User user = event.getUser();
        log.info("发送短信到{}，提示用户{}已修改密码",user.getPhone(),user.getUsername());
    }
}
