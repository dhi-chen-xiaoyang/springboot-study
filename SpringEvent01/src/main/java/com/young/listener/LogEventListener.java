package com.young.listener;

import com.young.event.UserChangePasswordLogEvent;
import com.young.event.UserChangePasswordMessageEvent;
import com.young.event.UserLoginLogEvent;
import com.young.event.UserLoginMessageEvent;
import com.young.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class LogEventListener {
    @EventListener({UserLoginLogEvent.class})
    @Async
    public void userLoginLogListener(UserLoginLogEvent event){
        User user = event.getUser();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("用户{}在{}登录了",user.getUsername(),user.getLoginTime());
    }

    @EventListener({UserChangePasswordLogEvent.class})
    public void userChangePasswordLogEvent(UserChangePasswordLogEvent event){
        User user = event.getUser();
        log.info("用户{}在{}修改了密码",user.getUsername(),user.getUpdateTime());
    }

}
