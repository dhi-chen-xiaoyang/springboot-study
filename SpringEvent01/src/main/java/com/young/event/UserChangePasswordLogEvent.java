package com.young.event;

import com.young.pojo.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;
@Getter
@Setter
public class UserChangePasswordLogEvent extends ApplicationEvent {
    private User user;
    public UserChangePasswordLogEvent(User user){
        super(new Object());
        this.user=user;
    }
}
