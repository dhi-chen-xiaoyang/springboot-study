package com.young.event;

import lombok.Data;

import java.io.Serializable;
@Data
public class LoginEvent implements Serializable {
    private static final long serialVersionUID=-1L;
    private String telePhone;
    private String email;
    private String goodsName;
}
