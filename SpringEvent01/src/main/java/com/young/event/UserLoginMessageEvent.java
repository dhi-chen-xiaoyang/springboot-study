package com.young.event;

import com.young.pojo.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Setter
@Getter
public class UserLoginMessageEvent extends ApplicationEvent {
    private User user;
    public UserLoginMessageEvent(User user){
        super(new Object());
        this.user=user;
    }
}
