package com.young.event;

import com.young.pojo.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class UserChangePasswordMessageEvent extends ApplicationEvent {
    private User user;
    public UserChangePasswordMessageEvent(User user){
        super(new Object());
        this.user=user;
    }
}
