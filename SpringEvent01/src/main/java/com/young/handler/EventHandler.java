package com.young.handler;

import com.young.listener.LoginListener;
import lombok.extern.slf4j.Slf4j;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class EventHandler {
    @Autowired
    private EventBus eventBus;
    @Autowired
    private LoginListener loginListener;

    /**
     * 注册总线
     */
    @PostConstruct
    public void init(){
        eventBus.register(loginListener);
    }

    /**
     * 注销总线
     */
    public void destroy(){
        eventBus.unregister(loginListener);
    }

    /**
     * 发布活动
     * @param obj 事件
     */
    public void eventPost(Object obj){
        eventBus.post(obj);
        log.info("分发消息完成============");
    }
}
