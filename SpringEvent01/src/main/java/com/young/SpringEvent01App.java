package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class SpringEvent01App {
    public static void main(String[] args) {
        SpringApplication.run(SpringEvent01App.class,args);
    }
}
