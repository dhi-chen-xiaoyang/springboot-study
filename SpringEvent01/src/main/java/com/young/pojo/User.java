package com.young.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName(value = "user")
public class User implements Serializable {
    private Integer id;
    private String username;
    private String password;
    private String phone;
    @TableField(exist = false)
    private Date loginTime;
    private Date updateTime;
}
