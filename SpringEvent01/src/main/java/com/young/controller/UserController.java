package com.young.controller;

import com.young.pojo.User;
import com.young.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public User login(@RequestParam("username")String username,
                      @RequestParam("password")String password){
        long start=System.currentTimeMillis();
        log.info("start:{}",start);
        User user = userService.login(username, password);
        long end=System.currentTimeMillis();
        log.info("end:{}",end);
        log.info("用时：{}",end-start);
        return user;
    }
}
