package com.young.controller;

import com.young.event.LoginEvent;
import com.young.handler.EventHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class EventController {
    @Autowired
    private EventHandler eventHandler;
    @GetMapping("/eventPost")
    public String eventPost(){
        LoginEvent loginEvent=new LoginEvent();
        loginEvent.setTelePhone("222222222");
        loginEvent.setEmail("123@qq.com");
        loginEvent.setGoodsName("沉河不浮");
        long start = System.currentTimeMillis();
        log.info("start:{}",start);
        eventHandler.eventPost(loginEvent);
        long end = System.currentTimeMillis();
        log.info("end:{}",end);
        log.info("用时：{}",end-start);
        return "ok";
    }
}
