package com.young.config;

import org.greenrobot.eventbus.EventBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;

@Configuration
public class EventConfig {
    @Resource
    private ThreadPoolTaskExecutor myExecutor;
    @Bean
    public EventBus eventBus(){
        return EventBus.builder().executorService(myExecutor.getThreadPoolExecutor()).build();
//        return EventBus.getDefault();
    }
}
