package com.young.service;

import com.young.pojo.User;
import com.young.request.ChangePasswordRequest;

public interface UserService {
    User login(String username,String password);
    Boolean updateUser(User user);
    Boolean changePassword(ChangePasswordRequest changePasswordRequest);
}
