package com.young.util;

import com.young.response.ConsultResponse;
import com.young.vo.ResultVO;

public class PayConsultHolder {
    public static ThreadLocal<ResultVO>stringThreadLocal=new ThreadLocal<>();

    public static void set(ResultVO value){
        stringThreadLocal.set(value);
    }

    public static ResultVO get(){
        return stringThreadLocal.get();
    }

    public static void remove(){
        stringThreadLocal.remove();
    }
}
