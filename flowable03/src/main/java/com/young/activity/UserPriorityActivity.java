package com.young.activity;

import com.young.response.ConsultResponse;
import com.young.util.PayConsultHolder;
import com.young.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserPriorityActivity implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        log.info("校验用户权限===============");
        ResultVO resultVO = PayConsultHolder.get();
        ConsultResponse consultResponse=(ConsultResponse) resultVO.getData();
        if (consultResponse.getUser().getRole()==null||consultResponse.getUser().getRole().equals("test")){
            throw new RuntimeException("用户没有权限===========================");
        }
    }
}
