package com.young.activity;

import com.alibaba.fastjson.JSONObject;
import com.young.converter.MainConverter;
import com.young.pojo.PayAccount;
import com.young.request.ConsultRequest;
import com.young.response.ConsultResponse;
import com.young.util.PayConsultHolder;
import com.young.vo.ResultVO;
import liquibase.pro.packaged.P;
import liquibase.sdk.Main;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CheckParamsActivity implements JavaDelegate {
    private static MainConverter mainConverter;

    @Autowired
    public void set(MainConverter mainConverter){
        CheckParamsActivity.mainConverter=mainConverter;
    }

    @Override
    public void execute(DelegateExecution delegateExecution) {
        log.info("校验参数=================");
        ConsultRequest consultRequest =JSONObject.parseObject(delegateExecution.getVariable("data").toString(),ConsultRequest.class);
        ConsultResponse consultResponse = mainConverter.consultRequest2ConsultResponse(consultRequest);
       checkPayoutAmount(consultResponse,consultRequest.getPayerId());
       checkPayinAmount(consultResponse,consultRequest.getPayeeId());
        ResultVO resultVo = ResultVO.success(consultResponse);
        PayConsultHolder.set(resultVo);
    }

    private void checkPayoutAmount(ConsultResponse consultResponse, String payerId) {
        PayAccount payAccount=getPayAmount(payerId);
        consultResponse.setPayout(payAccount);
    }

    private void checkPayinAmount(ConsultResponse consultResponse, String payerId) {
        PayAccount payAccount=getPayAmount(payerId);
        consultResponse.setPayin(payAccount);
    }

    private PayAccount getPayAmount(String payerId) {
        if (payerId==null){
            throw new RuntimeException("付款人或收款人为空");
        }
        PayAccount payAccount=new PayAccount();
        payAccount.setId(payerId);
        payAccount.setName("hello "+payerId);
        return payAccount;
    }
}
