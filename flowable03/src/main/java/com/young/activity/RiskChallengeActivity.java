package com.young.activity;

import com.alibaba.fastjson.JSONObject;
import com.young.pojo.RiskChallenge;
import com.young.request.ConsultRequest;
import com.young.response.ConsultResponse;
import com.young.util.PayConsultHolder;
import com.young.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class RiskChallengeActivity implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        log.info("风控挑战===================");
        ResultVO resultVO = PayConsultHolder.get();
        ConsultResponse consultResponse=(ConsultResponse) resultVO.getData();
        log.info("从配置文件中读取风控挑战==================");
        List<RiskChallenge> riskChallenges=new ArrayList<>();
        RiskChallenge risk1 = new RiskChallenge();
        risk1.setId("1");
        risk1.setRiskChallengeName("otp");
        RiskChallenge risk2 = new RiskChallenge();
        risk2.setId("2");
        risk2.setRiskChallengeName("password");
        riskChallenges.add(risk1);
        riskChallenges.add(risk2);
        consultResponse.setRiskChallenge(riskChallenges);
        resultVO.setData(consultResponse);
        PayConsultHolder.set(resultVO);
    }
}
