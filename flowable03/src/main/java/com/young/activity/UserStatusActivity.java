package com.young.activity;

import com.alibaba.fastjson.JSONObject;
import com.young.pojo.User;
import com.young.request.ConsultRequest;
import com.young.response.ConsultResponse;
import com.young.util.PayConsultHolder;
import com.young.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserStatusActivity implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        log.info("校验用户状态====================");
        ConsultRequest consultRequest = JSONObject.parseObject(delegateExecution.getVariable("data").toString(),ConsultRequest.class);
        String userId = consultRequest.getUserId();
        if(userId==null){
            throw new RuntimeException("用户不存在=============");
        }
        User user=getUser(userId);
        ResultVO resultVO = PayConsultHolder.get();
        ConsultResponse consultResponse=(ConsultResponse) resultVO.getData();
        consultResponse.setUser(user);
    }

    private User getUser(String userId) {
        User user=new User();
        user.setId(userId);
        user.setUsername("dhi");
        user.setRole("user");
        user.setIsEnabled(true);
        if (userId.equals("1")||userId.equals("2")){
            throw new RuntimeException("用户被禁用+=================");
        }
        if (userId.equals("3")){
            user.setRole("test");
        }
        return user;
    }
}
