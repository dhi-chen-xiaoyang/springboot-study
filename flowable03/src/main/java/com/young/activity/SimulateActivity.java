package com.young.activity;

import com.alibaba.fastjson.JSONObject;
import com.young.pojo.PayOption;
import com.young.pojo.RiskChallenge;
import com.young.request.ConsultRequest;
import com.young.response.ConsultResponse;
import com.young.util.PayConsultHolder;
import com.young.vo.ResultVO;
import liquibase.pro.packaged.P;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SimulateActivity implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        log.info("支付试算======================");
        ConsultRequest consultRequest = JSONObject.parseObject(delegateExecution.getVariable("data").toString(),ConsultRequest.class);
        List<PayOption>payOptions=readPayOptionList();
        if (consultRequest.getSimulate()){
            payOptions=simulate(payOptions,consultRequest.getMoney());
        }
        ResultVO resultVO = PayConsultHolder.get();
        ConsultResponse consultResponse=(ConsultResponse) resultVO.getData();
        consultResponse.setPayOption(payOptions);
    }

    private List<PayOption> readPayOptionList() {
        log.info("从配置中读取支付选项=====================");
        List<PayOption>payOptions=new ArrayList<>();
        PayOption wechat=new PayOption();
        wechat.setId("1");
        wechat.setName("微信支付");
        wechat.setBalance(1000);
        wechat.setIsAsset(true);
        payOptions.add(wechat);
        PayOption aliPay=new PayOption();
        aliPay.setId("2");
        aliPay.setName("支付宝");
        aliPay.setBalance(2000);
        aliPay.setIsAsset(true);
        payOptions.add(aliPay);
        PayOption jd=new PayOption();
        jd.setId("3");
        jd.setName("京东白条");
        jd.setIsAsset(false);
        payOptions.add(jd);
        PayOption bank=new PayOption();
        bank.setId("4");
        bank.setName("银联");
        bank.setBalance(5000);
        bank.setIsAsset(true);
        payOptions.add(bank);
        return payOptions;
    }

    private List<PayOption> simulate(List<PayOption>payOptions, Integer money){
       log.info("计算有那些可用资产==================");
        return payOptions.stream().filter(PayOption::getIsAsset)
                .filter(payOption -> {
                    return payOption.getBalance() >= money;
                }).collect(Collectors.toList());

    }
}
