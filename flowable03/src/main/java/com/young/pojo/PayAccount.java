package com.young.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class PayAccount implements Serializable {
    private String id;
    private String name;
}
