package com.young.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "money_expense")
public class MoneyExpense {
    @TableId(type = IdType.INPUT)
    private String id;
    private String userId;
    private String userName;
    private String processInstanceId;
    private String taskId;
    private Integer money;
    private String description;
    private Long applyTime;
    private String assigneeId;
    private String assigneeName;
    private Long assigneeTime;
    private String status;
}
