package com.young.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class RiskChallenge implements Serializable {
    private String id;
    private String riskChallengeName;
}
