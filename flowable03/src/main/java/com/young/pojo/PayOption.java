package com.young.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class PayOption implements Serializable {
    private String id;
    private String name;
    private Integer balance;
    private Boolean isAsset;
}
