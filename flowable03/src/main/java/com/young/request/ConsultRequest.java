package com.young.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class ConsultRequest implements Serializable {
    private String userId;
    private String payerId;
    private String payeeId;
    private String payOptionId;
    private Integer money;
    private String requestRefNo;
    private String cashierToken;
    private Boolean simulate;
    private Long consultTime;
}
