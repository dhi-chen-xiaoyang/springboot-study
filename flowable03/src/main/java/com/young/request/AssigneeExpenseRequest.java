package com.young.request;

import lombok.Data;

@Data
public class AssigneeExpenseRequest {
    private String taskId;
    private String assigneeId;
    private String assigneeName;
}
