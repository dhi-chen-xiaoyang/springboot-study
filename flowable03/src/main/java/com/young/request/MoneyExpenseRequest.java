package com.young.request;

import lombok.Data;

@Data
public class MoneyExpenseRequest {
    private String userId;
    private String userName;
    private Integer money;
    private String description;
}
