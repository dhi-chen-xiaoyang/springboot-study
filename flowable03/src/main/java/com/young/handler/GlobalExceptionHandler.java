package com.young.handler;

import com.young.util.PayConsultHolder;
import com.young.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    public void handlerRuntimeException(RuntimeException e){
        log.error("error:{}",e.getMessage());
        ResultVO fail = ResultVO.fail(e.getMessage());
        PayConsultHolder.set(fail);
    }

    @ExceptionHandler(Exception.class)
    public void handlerException(RuntimeException e){
        log.error("error:{}",e.getMessage());
        ResultVO fail = ResultVO.fail(e.getMessage());
        PayConsultHolder.set(fail);
    }
}
