package com.young.engine;

import com.alibaba.fastjson.JSONObject;
import com.young.request.ConsultRequest;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class FlowableEngine {
    @Autowired
    private RuntimeService runtimeService;

    public void start(String processKey, ConsultRequest consultRequest){
        if (consultRequest==null){
            throw new RuntimeException("咨询请求为空=================");
        }
        log.info("启动流程引擎===================");
        Map<String,Object>map=new HashMap<>();
        map.put("data",JSONObject.toJSON(consultRequest));
        runtimeService.startProcessInstanceByKey(processKey, map);
    }
}
