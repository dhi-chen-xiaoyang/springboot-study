package com.young.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.young.pojo.MoneyExpense;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MoneyExpenseMapper extends BaseMapper<MoneyExpense> {
}
