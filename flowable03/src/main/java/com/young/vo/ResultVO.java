package com.young.vo;

import liquibase.pro.packaged.R;
import lombok.Data;

import java.io.Serializable;

@Data
public class ResultVO <T> implements Serializable {
    private Integer code;
    private String msg;
    private T data;

    public ResultVO(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public ResultVO(Integer code,String msg,T data){
        this.code=code;
        this.msg=msg;
        this.data=data;
    }

    public static ResultVO fail(String msg){
        return new ResultVO(400,msg);
    }

    public static <T> ResultVO success(T data){
        return new ResultVO(200,"操作成功",data);
    }
}
