package com.young.converter;

import com.young.pojo.MoneyExpense;
import com.young.request.ConsultRequest;
import com.young.request.MoneyExpenseRequest;
import com.young.response.ConsultResponse;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
public interface MainConverter {
    MoneyExpense moneyExpenseRequest2MoneyExpense(MoneyExpenseRequest moneyExpenseRequest);
    ConsultResponse consultRequest2ConsultResponse(ConsultRequest consultRequest);
}
