package com.young.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.young.converter.MainConverter;
import com.young.enums.StatusEnum;
import com.young.mapper.MoneyExpenseMapper;
import com.young.pojo.MoneyExpense;
import com.young.request.MoneyExpenseRequest;
import com.young.request.AssigneeExpenseRequest;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class MoneyExpenseService {
    @Autowired
    private MoneyExpenseMapper moneyExpenseMapper;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private MainConverter mainConverter;
    @Autowired
    private TaskService taskService;

   public String addMoneyExpense(MoneyExpenseRequest moneyExpenseRequest){
       HashMap<String,Object>map=new HashMap<>();
       map.put("taskUser",moneyExpenseRequest.getUserId());
       map.put("money",moneyExpenseRequest.getMoney());
       ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("Expense", map);
       MoneyExpense moneyExpense = mainConverter.moneyExpenseRequest2MoneyExpense(moneyExpenseRequest);
       //设置流程id
       moneyExpense.setProcessInstanceId(processInstance.getId());
       moneyExpense.setApplyTime(System.currentTimeMillis());
       //获取taskId
       Task task = taskService.createTaskQuery().taskAssignee(moneyExpense.getUserId()).orderByTaskCreateTime().desc().list().get(0);
       moneyExpense.setTaskId(task.getId());
       moneyExpense.setStatus(StatusEnum.INIT.name());
       moneyExpense.setId(processInstance.getId());
       moneyExpenseMapper.insert(moneyExpense);
       return moneyExpense.getId();
   }

   public MoneyExpense getMoneyExpenseById(String id){
       return moneyExpenseMapper.selectById(id);
   }

    public String rejectMoneyExpense(AssigneeExpenseRequest assigneeExpenseRequest) {
        Task task = taskService.createTaskQuery().taskId(assigneeExpenseRequest.getTaskId()).singleResult();
        if (task==null){
            throw new RuntimeException("流程不存在");
        }

        Map<String,Object>map=new HashMap<>();
        map.put("outcome","驳回");
        taskService.complete(assigneeExpenseRequest.getTaskId(),map);
        MoneyExpense moneyExpense = getMoneyExpenseByProcessInstanceId(task.getProcessInstanceId());
        moneyExpense.setApplyTime(System.currentTimeMillis());
        moneyExpense.setAssigneeId(assigneeExpenseRequest.getAssigneeId());
        moneyExpense.setAssigneeName(assigneeExpenseRequest.getAssigneeName());
        moneyExpense.setAssigneeTime(System.currentTimeMillis());
        moneyExpense.setStatus(StatusEnum.FAIL.name());
        moneyExpenseMapper.updateById(moneyExpense);
        return "success";
    }

    public String applyMoneyExpense(AssigneeExpenseRequest assignassigneeExpenseRequesteExpenseSDO) {
        Task task = taskService.createTaskQuery().taskId(assignassigneeExpenseRequesteExpenseSDO.getTaskId()).singleResult();
        if (task==null){
            throw new RuntimeException("流程不存在");
        }

        Map<String,Object>map=new HashMap<>();
        map.put("outcome","通过");
        taskService.complete(assignassigneeExpenseRequesteExpenseSDO.getTaskId(),map);
        MoneyExpense moneyExpense = getMoneyExpenseByProcessInstanceId(task.getProcessInstanceId());
        moneyExpense.setApplyTime(System.currentTimeMillis());
        moneyExpense.setAssigneeId(assignassigneeExpenseRequesteExpenseSDO.getAssigneeId());
        moneyExpense.setAssigneeName(assignassigneeExpenseRequesteExpenseSDO.getAssigneeName());
        moneyExpense.setAssigneeTime(System.currentTimeMillis());
        moneyExpense.setStatus(StatusEnum.SUCCESS.name());
        moneyExpenseMapper.updateById(moneyExpense);
        return "success";
    }

    private MoneyExpense getMoneyExpenseByProcessInstanceId(String processInstanceId){
        LambdaQueryWrapper<MoneyExpense>queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(MoneyExpense::getProcessInstanceId,processInstanceId);
        return moneyExpenseMapper.selectOne(queryWrapper);
    }
}
