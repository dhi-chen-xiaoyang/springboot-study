package com.young.response;

import com.young.pojo.PayAccount;
import com.young.pojo.PayOption;
import com.young.pojo.RiskChallenge;
import com.young.pojo.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ConsultResponse implements Serializable {
    private User user;
    private PayAccount payout;
    private PayAccount payin;
    private List<PayOption> payOption;
    private List<RiskChallenge> riskChallenge;
    private Integer money;
    private String requestRefNo;
    private String cashierToken;
    private Boolean simulate;
    private Long consultTime;
}
