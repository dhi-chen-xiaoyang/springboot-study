package com.young;

import com.young.engine.FlowableEngine;
import com.young.pojo.PayOption;
import com.young.request.ConsultRequest;
import com.young.util.PayConsultHolder;
import com.young.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
@Slf4j
public class Flowable03ApplicationTest {
    @Autowired
    private FlowableEngine flowableEngine;

    @Test
    public void testOk(){
        ConsultRequest consultRequest=new ConsultRequest();
        consultRequest.setUserId("4");
        consultRequest.setPayerId("1");
        consultRequest.setPayeeId("2");
        consultRequest.setPayOptionId("3");
        consultRequest.setMoney(100);
        String token = UUID.randomUUID().toString();
        consultRequest.setRequestRefNo(token);
        consultRequest.setCashierToken(token);
        consultRequest.setSimulate(true);
        consultRequest.setConsultTime(System.currentTimeMillis());
        flowableEngine.start("consult",consultRequest);
        ResultVO resultVO = PayConsultHolder.get();
        log.info("resultVO:{}",resultVO);
        System.out.println(resultVO);
    }

    @Test
    public void test01(){
        ConsultRequest consultRequest=new ConsultRequest();
        consultRequest.setPayerId("1");
        consultRequest.setPayeeId("2");
        consultRequest.setPayOptionId("3");
        consultRequest.setMoney(100);
        String token = UUID.randomUUID().toString();
        consultRequest.setRequestRefNo(token);
        consultRequest.setCashierToken(token);
        consultRequest.setSimulate(true);
        consultRequest.setConsultTime(System.currentTimeMillis());
        flowableEngine.start("consult",consultRequest);
    }
}
