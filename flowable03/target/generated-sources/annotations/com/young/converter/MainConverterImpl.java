package com.young.converter;

import com.young.pojo.MoneyExpense;
import com.young.request.ConsultRequest;
import com.young.request.MoneyExpenseRequest;
import com.young.response.ConsultResponse;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-22T16:53:13+0800",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 1.8.0_301 (Oracle Corporation)"
)
@Component
public class MainConverterImpl implements MainConverter {

    @Override
    public MoneyExpense moneyExpenseRequest2MoneyExpense(MoneyExpenseRequest moneyExpenseRequest) {
        if ( moneyExpenseRequest == null ) {
            return null;
        }

        MoneyExpense moneyExpense = new MoneyExpense();

        moneyExpense.setUserId( moneyExpenseRequest.getUserId() );
        moneyExpense.setUserName( moneyExpenseRequest.getUserName() );
        moneyExpense.setMoney( moneyExpenseRequest.getMoney() );
        moneyExpense.setDescription( moneyExpenseRequest.getDescription() );

        return moneyExpense;
    }

    @Override
    public ConsultResponse consultRequest2ConsultResponse(ConsultRequest consultRequest) {
        if ( consultRequest == null ) {
            return null;
        }

        ConsultResponse consultResponse = new ConsultResponse();

        consultResponse.setMoney( consultRequest.getMoney() );
        consultResponse.setRequestRefNo( consultRequest.getRequestRefNo() );
        consultResponse.setCashierToken( consultRequest.getCashierToken() );
        consultResponse.setSimulate( consultRequest.getSimulate() );
        consultResponse.setConsultTime( consultRequest.getConsultTime() );

        return consultResponse;
    }
}
