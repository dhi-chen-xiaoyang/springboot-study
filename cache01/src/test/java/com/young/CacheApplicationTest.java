package com.young;

import com.young.pojo.UserInfo;
import com.young.service.UserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class CacheApplicationTest {
    @Autowired
    private UserInfoService userInfoService;

    @Test
    public void getUserInfo(){
        UserInfo userInfo = userInfoService.getById(1);
       log.info("userInfo:{}",userInfo);
        userInfo=userInfoService.getById(1);
        log.info("userInfo:{}",userInfo);
    }

    @Test
    public void updateUserInfo(){
        UserInfo userInfo=userInfoService.getById(2);
        log.info("userInfo:{}",userInfo);
        userInfo=new UserInfo();
        userInfo.setSex("female");
        userInfo.setId(2);
//        userInfo.setSex("female");
        userInfoService.updateUserInfo(userInfo);
        userInfo=userInfoService.getById(2);
        log.info("userInfo:{}",userInfo);
    }

    @Test
    public void deleteUserInfo(){
        UserInfo userInfo=userInfoService.getById(6);
        log.info("userInfo:{}",userInfo);
        userInfoService.deleteById(6);
        userInfo=userInfoService.getById(2);
        log.info("userInfo:{}",userInfo);
    }

    @Test
    public void getUserInfo1()throws InterruptedException{
        UserInfo userInfo = userInfoService.getById(1);
        log.info("第一次获取：{}",userInfo);
        Thread.sleep(30000);
        userInfo=userInfoService.getById(1);
        log.info("第二次获取：{}",userInfo);
        userInfo=userInfoService.getById(1);
        log.info("第三次获取：{}",userInfo);
    }

    @Test
    public void updateUserInfo1()throws InterruptedException{
        UserInfo userInfo = userInfoService.getById(1);
        log.info("第一次获取:{}",userInfo);
        userInfo.setSex("no female");
        userInfoService.updateUserInfo(userInfo);
        userInfo=userInfoService.getById(1);
        log.info("第二次获取:{}",userInfo);
        userInfo=userInfoService.getById(1);
        log.info("第三次获取：{}",userInfo);
    }

}
