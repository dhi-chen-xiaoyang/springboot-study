package com.young.service;

import com.young.pojo.UserInfo;

public interface UserInfoService {
    void addUserInfo(UserInfo userInfo);
    UserInfo getById(Integer id);
    UserInfo updateUserInfo(UserInfo userInfo);
    void deleteById(Integer id);
}
