package com.young.config;

import io.lettuce.core.RedisClient;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedisConfig {
    public static final String USER_CACHE="user:";
    public static final String USER_LOCK_CACHE="user:lock:";

    public static final Long LEAST_EXPIRE_TIME=30000L;
    public static final Long EXPIRE_TIME=120000L;

    @Bean
    public Redisson redisson(){
        Config config=new Config();
        config.useSingleServer().setAddress("redis://localhost:6379").setDatabase(0);
        return (Redisson) Redisson.create(config);
    }
}
