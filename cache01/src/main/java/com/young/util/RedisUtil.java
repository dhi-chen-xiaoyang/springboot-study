package com.young.util;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {
    @Resource
    private RedisTemplate<String,Object>redisTemplate;

    public void set(String key,Object value,long timeout){
        redisTemplate.opsForValue().set(key,value,timeout, TimeUnit.SECONDS);
    }

    public Object get(String key){
        return redisTemplate.opsForValue().get(key);
    }

    public void expire(String key,long timeout){
        redisTemplate.expire(key,timeout,TimeUnit.SECONDS);
    }

    public void remove(String key){
        redisTemplate.delete(key);
    }
}
