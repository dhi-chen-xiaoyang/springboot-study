package com.young.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@TableName(value = "t_user")
public class UserInfo implements Serializable {
    private Integer id;
    private String name;
    private String sex;
    private Integer age;
}
