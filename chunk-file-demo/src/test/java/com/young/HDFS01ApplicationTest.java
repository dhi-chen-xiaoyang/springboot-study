package com.young;

import com.young.config.DirConfiguration;
import com.young.service.ChunkFileService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class HDFS01ApplicationTest {
    @Resource
    private ChunkFileService chunkFileService;
    @Resource
    private DirConfiguration dirConfiguration;
//    @Test
//    public void testDirConfiguration(){
//        chunkFileService.splitFile("","",0);
//    }
    //测试分片
    @Test
    public void testSplit(){
        String identifier = chunkFileService.getIdentifier("D:\\root\\dhi\\client\\dhi.pdf");
        String result = chunkFileService.splitFile("D:\\root\\dhi\\client\\dhi.pdf", dirConfiguration.getSource(), identifier, 5);
        System.out.println("result:"+result);
    }
    @Test
    public void getIdentifier(){
        String identifier = chunkFileService.getIdentifier("D:\\root\\dhi\\client\\dhi.pdf");
        System.out.println("identifier:"+identifier);
    }
}
