package com.young.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName(value = "chunk_file")
public class ChunkFile {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String path;
    private String fileName;
    private String suffix;
    private Integer totalSize;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private Integer chunkIndex;
    private Integer chunkSize;
    private Integer totalChunks;
    private String identifier;
}
