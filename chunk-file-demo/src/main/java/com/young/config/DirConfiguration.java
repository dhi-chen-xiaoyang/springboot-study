package com.young.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class DirConfiguration {
    @Value("${dir.source}")
    private String source;
    @Value("${dir.temp}")
    private String temp;
    @Value("${dir.target}")
    private String target;
}
