package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HDFS01Application {
    public static void main(String[] args) {
        SpringApplication.run(HDFS01Application.class,args);
    }
}
