package com.young.controller;

import com.young.dto.Result;
import com.young.service.ChunkFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@Slf4j
public class ChunkFileController {
    @Resource
    private ChunkFileService chunkFileService;
    //上传分片文件
    @PostMapping(value = "/uploadChunk")
    public Result uploadChunk(@RequestPart MultipartFile file,
                              @RequestParam(name = "identifier")String identifier,
                              @RequestParam(name = "fileName")String fileName,
                              @RequestParam(name = "suffix")String suffix,
                              @RequestParam(name = "index")Integer chunkIndex,
                              @RequestParam(name = "total")Integer chunkTotal,
                              @RequestParam(name = "size")Integer chunkSize,
                              @RequestParam(name = "totalSize")Integer totalSize){
        log.info("上传分片文件==================");
        return chunkFileService.uploadChunk(file,identifier,fileName,suffix,chunkIndex,chunkTotal,chunkSize,totalSize);
    }
    @GetMapping(value = "/verify")
    public Result verify(@RequestParam(name = "identifier")String identifier){
        return chunkFileService.verify(identifier);
    }
    @GetMapping(value = "/merge")
    public Result merge(@RequestParam(name = "identifier")String identifier){
        return chunkFileService.merge(identifier);
    }
}
