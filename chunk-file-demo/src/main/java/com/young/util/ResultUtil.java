package com.young.util;

import com.young.dto.Result;

public class ResultUtil {
    public static Result success(){
        return new Result<>(200,"操作成功");
    }
    public static Result success(Object data){
        return new Result<>(200,"操作成功",data);
    }
    public static Result fail(){
        return new Result<>(400,"操作失败");
    }
    public static Result fail(Integer code,String msg){
        return new Result<>(code,msg);
    }
    public static Result fail(Integer code,String msg,Object data){
        return new Result<>(code,msg,data);
    }
}
