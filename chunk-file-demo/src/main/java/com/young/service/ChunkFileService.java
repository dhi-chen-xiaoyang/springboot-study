package com.young.service;

import com.young.dto.Result;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface ChunkFileService {
    //文件分片
    String splitFile(String sourceFile,String dir,String identifier,int count);
    //文件合并
    String getIdentifier(String filename);

    Result merge(String identifier);

    Result verify(String identifier);

    Result uploadChunk(MultipartFile file, String identifier,String fileName, String suffix, Integer chunkIndex, Integer chunkTotal, Integer chunkSize,Integer totalSize);
}
