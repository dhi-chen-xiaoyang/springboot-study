package com.young.util;

import java.util.Random;

public class CodeUtill {
    public static String getCode(){
        char[]chars="0123456789".toCharArray();
        int n=4;
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<n;i++){
            sb.append(chars[new Random().nextInt(chars.length)]);
        }
        return sb.toString();
    }
}
