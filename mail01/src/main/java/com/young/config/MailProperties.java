package com.young.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "spring.mail")
@Component
public class MailProperties {
    private String username;
    public void setUsername(String username){
        this.username=username;
    }
    public String getUsername(){
        return this.username;
    }
}
