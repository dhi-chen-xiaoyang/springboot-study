package com.young;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.young.pojo.User;
import com.young.service.UserService;
import com.young.service.UserService1;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class MybatisPlusAppTest {
    @Autowired
    private UserService userService;

    @Test
    public void testLessThanBirthday(){
        Date birthday=new Date(102,4,15);
        Page<User> page = userService.getUserPageLessThanBirthday(1, 5, birthday);
        if (page!=null){
            page.getRecords().stream()
                    .forEach(System.out::println);
        }
    }

    @Autowired
    private UserService1 userService1;

    @Test
    public void testBatchInsert(){
        List<User>list=new ArrayList<>();
        for(int i=6;i<10;i++){
            User user=new User();
            user.setUsername("test"+i);
            user.setPassword("123456");
            user.setHeight(166+i);
            user.setWeight(59+i);
            user.setRegisterTime(new Date());
            user.setBirthday(new Date());
            list.add(user);
        }
        userService1.saveBatch(list);
    }
}
