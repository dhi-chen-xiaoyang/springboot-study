package com.young.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.young.pojo.User;
import com.young.repository.UserRepository;
import com.young.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public Integer saveUser(User user) {
        int id = userRepository.saveUser(user);
        if (id<=0){
            throw new RuntimeException("用户添加失败");
        }
        //添加成功，返回主键
       return id;
    }

    @Override
    public User getUserById(Integer id) {
        User user = userRepository.getUserById(id);
        //屏蔽掉密码
        user.setPassword(null);
        return user;
    }

    @Override
    public Page<User> getUserPageLessThanBirthday(Integer pageNum, Integer pageSize, Date birthday) {
       Page<User>page=userRepository.getUserPageLessThanBirthday(pageNum,pageSize,birthday);
        if (page!=null){
            //屏蔽掉密码
            page.getRecords().stream()
                    .forEach(user->{
                        user.setPassword(null);
                    });
        }
        return page;
    }

    @Override
    public Page<User> getUserPageGreaterThanBirthday(Integer pageNum, Integer pageSize, Date birthday) {
        Page<User>page=userRepository.getUserPageGreaterThanBirthday(pageNum,pageSize,birthday);
        if (page!=null){
            //屏蔽掉密码
            page.getRecords().stream()
                    .forEach(user->{
                        user.setPassword(null);
                    });
        }
        return page;
    }
}
