package com.young.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.young.pojo.User;

import java.util.Date;
import java.util.List;

public interface UserService {
    Integer saveUser(User user);
    User getUserById(Integer id);
    Page<User> getUserPageLessThanBirthday(Integer pageNum, Integer pageSize, Date birthday);
    Page<User>getUserPageGreaterThanBirthday(Integer pageNum,Integer pageSize,Date birthday);
}
