package com.young.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.young.mapper.UserMapper;
import com.young.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class UserRepository {
    @Autowired
    private UserMapper userMapper;

    public Integer saveUser(User user){
        int insert = userMapper.insert(user);
        if (insert>0){
            //成功，返回用户主键
            return user.getId();
        }
        return -1;
    }

    public User getUserById(Integer id){
        return userMapper.selectById(id);
    }

    public Page<User> getUserPageLessThanBirthday(Integer pageNum, Integer pageSize, Date birthday) {
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.lt(User::getBirthday,birthday);
        Page<User>page=new Page<>(pageNum,pageSize);
        return userMapper.selectPage(page,queryWrapper);
    }

    public Page<User> getUserPageGreaterThanBirthday(Integer pageNum, Integer pageSize, Date birthday) {
        LambdaQueryWrapper<User>queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.gt(User::getBirthday,birthday);
        Page<User>page=new Page<>(pageNum,pageSize);
        return userMapper.selectPage(page,queryWrapper);
    }
}
