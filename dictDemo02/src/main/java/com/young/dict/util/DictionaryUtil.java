package com.young.dict.util;

import com.young.dict.entity.Dictionary;
import com.young.dict.service.DictionaryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class DictionaryUtil {
    public static Map<String, Dictionary>map=new ConcurrentHashMap<>();
    @Autowired
    private DictionaryDao dictionaryDao;
    @PostConstruct
    public void init(){
        System.out.println("系统启动中==========");
        List<Dictionary> list= dictionaryDao.getDictionary();
        for (Dictionary dictionary : list) {
            map.put(dictionary.getQueryKey(),dictionary);
        }
    }
    public Dictionary getDictionary(String key){
        Dictionary dictionary = map.get(key);
        if (dictionary==null){
            dictionary=dictionaryDao.queryDictionaryByKey(key);
            if (dictionary!=null){
                map.put(key,dictionary);
            }
        }
        return dictionary;
    }
    @PreDestroy
    public void destroy(){
        System.out.println("系统运行结束");
    }
}
