package com.young.dict.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.young.dict.entity.Dictionary;
import com.young.dict.mapper.DictionaryMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DictionaryDao {
    @Autowired
    private DictionaryMapper dictionaryMapper;
    public List<Dictionary>getDictionary(){
        return dictionaryMapper.selectList(null);
    }
    public Dictionary queryDictionaryByKey(String key){
        LambdaQueryWrapper<Dictionary>queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Dictionary::getQueryKey,key);
        return dictionaryMapper.selectOne(queryWrapper);
    }
}
