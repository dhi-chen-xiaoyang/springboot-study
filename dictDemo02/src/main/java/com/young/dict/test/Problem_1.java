package com.young.dict.test;

import java.util.Random;

public class Problem_1 {
    public static void main(String[] args) {
        int[] narr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
        int[] lot = lot(narr, 16);
        for (int i: lot) {
            System.out.print(i + " ");
        }
    }

    private static int[] lot(int[] narr, int m) {
        int n = narr.length;
        int[] ret = new int[m];
        Random random = new Random();
        int lastValidIdx = n - 1;
        for (int i = 0; i < m; i++) {
            int idx = random.nextInt(m - i);
            ret[i] = narr[idx];
            swap(narr, idx, lastValidIdx--);
        }
        return ret;
    }

    private static void swap(int[] narr, int a, int b) {
        int temp = narr[a];
        narr[a] = narr[b];
        narr[b] = temp;
    }
}