package com.young.dict.test;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Student implements Cloneable, Serializable {
    private int id;
    public Student(){
    }
    public Student(Integer id){
        this.id=id;
    }
    @Override
    protected Object clone()throws CloneNotSupportedException{
        return super.clone();
    }
    @Override
    public String toString(){
        return "Student[id="+id+"]";
    }

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, InvocationTargetException, CloneNotSupportedException, IOException, NoSuchMethodException, ClassNotFoundException {
        System.out.println("new 吵架呢");
        Student stu1=new Student(123);
        System.out.println(stu1);
        System.out.println("\n-----------\n");
        System.out.println("使用Class newInstance");
        Student stu2=Student.class.newInstance();
        System.out.println(stu2);
        System.out.println("\n---------\n");
        System.out.println("Constructor");
        Constructor<Student> constructor = Student.class.getConstructor(Integer.class);
        Student stu3=constructor.newInstance(123);
        System.out.println(stu3);
        System.out.println("\n----------\n");
        System.out.println("Clone");
        Student stu4=(Student)stu3.clone();
        System.out.println(stu4);
        System.out.println("\n-----------\n");
        System.out.println("反序列化");
        ObjectOutputStream output=new ObjectOutputStream(new FileOutputStream("student.bin"));
        output.writeObject(stu4);
        output.close();
        ObjectInputStream input=new ObjectInputStream(new FileInputStream("student.bin"));
        Student stu5=(Student)input.readObject();
        System.out.println(stu5);
    }
}
