package com.young.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.young.dict.entity.Dictionary;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DictionaryMapper extends BaseMapper<Dictionary> {
}
