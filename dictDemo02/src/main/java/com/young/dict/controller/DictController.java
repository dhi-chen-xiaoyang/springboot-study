package com.young.dict.controller;

import com.young.dict.entity.Dictionary;
import com.young.dict.util.DictionaryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DictController {
    @Autowired
    private DictionaryUtil dictionaryUtil;
    @GetMapping("/test")
    public Dictionary getDictionary(@RequestParam("key")String key){
        return dictionaryUtil.getDictionary(key);
    }
}
