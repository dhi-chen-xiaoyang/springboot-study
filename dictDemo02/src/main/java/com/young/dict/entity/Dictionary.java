package com.young.dict.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "t_dictionary")
public class Dictionary {
    @TableId("code")
    private String code;
    private String name;
    private String queryKey;
    private String keyZh;
    private String message;
//    @TableLogic
    private Integer isEnable;
}
