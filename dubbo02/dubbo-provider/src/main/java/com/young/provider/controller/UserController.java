package com.young.provider.controller;

import com.young.api.entity.User;
import com.young.api.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
public class UserController {
    @Autowired
    private IUserService userService;
    @GetMapping("/selectUserById/{id}")
    public User selectUserById(@PathVariable("id")Long id){
        return userService.selectUserById(id);
    }
}
