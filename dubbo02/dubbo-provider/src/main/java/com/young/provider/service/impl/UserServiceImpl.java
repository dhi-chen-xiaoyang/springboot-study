package com.young.provider.service.impl;

import com.young.api.entity.User;
import com.young.api.service.IUserService;
import com.young.provider.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@DubboService(loadbalance = "roundrobin")
@Service
@Slf4j
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Value("${server.port}")
    private Integer port;
    @Override
    public User selectUserById(Long id){
        log.info("当前访问的提供者的port是：{}",port);
        User user=userMapper.selectUserById(id);
        return user;
    }

    @Override
    public User asyncSelectUserById(Long id) {
        log.info("异步访问获取用户,port:{}",port);
        try{
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        return userMapper.selectUserById(id);
    }

    @Override
    public User syncSelectUserById(Long id) {
        log.info("同步访问获取用户,port:{}",port);
        try{
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        return userMapper.selectUserById(id);
    }

    @Override
    public User retrySelectUserById(Long id) {
        log.info("超时重传获取用户id,port:{}",port);
        try{
            Thread.sleep(4000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        User user = userMapper.selectUserById(id);
        log.info("user:{}",user);
        return user;
    }
}
