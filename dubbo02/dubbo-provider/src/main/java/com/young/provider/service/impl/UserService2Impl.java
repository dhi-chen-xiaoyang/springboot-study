package com.young.provider.service.impl;

import com.young.api.entity.User;
import com.young.api.service.IUserService2;
import com.young.provider.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@DubboService
@Slf4j
public class UserService2Impl implements IUserService2 {
    @Autowired
    private UserMapper userMapper;
    @Value("${server.port}")
    private Integer port;
    @Override
    public User getUserById(Long id) {
        log.info("超时重传获取用户id,port:{}",port);
        try{
            Thread.sleep(4000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        User user = userMapper.selectUserById(id);
        log.info("user:{}",user);
        return user;
    }
}
