package com.young.api.service;

import com.young.api.entity.User;

public interface IUserService {
    User selectUserById(Long id);
    User asyncSelectUserById(Long id);
    User syncSelectUserById(Long id);
    User retrySelectUserById(Long id);
}
