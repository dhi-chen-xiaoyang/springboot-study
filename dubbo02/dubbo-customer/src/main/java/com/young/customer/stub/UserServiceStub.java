package com.young.customer.stub;

import com.young.api.entity.User;
import com.young.api.service.IUserService2;

public class UserServiceStub implements IUserService2 {
    private IUserService2 userService2;
    public UserServiceStub(IUserService2 userService2){
        this.userService2=userService2;
    }
    @Override
    public User getUserById(Long id) {
        try{
            //检查参数是否有误
            if (id<=0){
                System.out.println("参数有误");
                return null;
            }
            return userService2.getUserById(id);
        }catch (Exception e){
//            e.printStackTrace();
            System.out.println("启动容错===========");
            User user=new User();
            user.setId(0L);
            user.setUsername("容错");
            user.setPassword("本地存根");
            return user;
        }
    }
}
