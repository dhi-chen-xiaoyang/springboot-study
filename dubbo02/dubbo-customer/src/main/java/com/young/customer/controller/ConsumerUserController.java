package com.young.customer.controller;

import com.young.api.entity.User;
import com.young.api.service.IUserService;
import com.young.api.service.IUserService2;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.Method;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/consumer")
@Slf4j
public class ConsumerUserController {
    @DubboReference(protocol = "dubbo",loadbalance = "random",
    methods = {@Method(name = "asyncSelectUserById",async = true),
    @Method(name = "retrySelectUserById",retries = 3,timeout = 2000)})
    private IUserService userService;
    @DubboReference(stub = "com.young.customer.stub.UserServiceStub")
    private IUserService2 userService2;
    @RequestMapping("/stubSelectUserById/{id}")
    public User stubSelectUserById(@PathVariable("id")Long id){
        return userService2.getUserById(id);
    }
    @RequestMapping("/retrySelectUserById/{id}")
    public User retrySelectUserById(@PathVariable("id")Long id){
        User user = userService.retrySelectUserById(id);
        log.info("retryUser:{}",user);
        return user;
    }
    @RequestMapping("/selectUserById/{id}")
    public User getUser(@PathVariable("id")Long id){
        User user=userService.selectUserById(id);
        log.info("response from provider:{}",user);
        return user;
    }
    @RequestMapping("/syncSelectUserById/{id}")
    public User asyncGetUser(@PathVariable("id")Long id){
        long start = System.currentTimeMillis();
        User user=null;
        user=userService.syncSelectUserById(id);
        user=userService.syncSelectUserById(id);
        user=userService.syncSelectUserById(id);
        long end=System.currentTimeMillis();
        log.info("用时：{}",end-start);
        return user;
    }
    @RequestMapping("/asyncSelectUserById/{id}")
    public User asyncGetUser2(@PathVariable("id")Long id){
        long start = System.currentTimeMillis();
        User user=null;
        userService.asyncSelectUserById(id);
//        Future<User> future1 = RpcContext.getContext().getFuture();
        Future<User> future1 = RpcContext.getServerContext().getFuture();
        userService.asyncSelectUserById(id);
//        Future<User> future2 = RpcContext.getContext().getFuture();
        Future<User>future2=RpcContext.getServerContext().getFuture();
        userService.asyncSelectUserById(id);
//        Future<User> future3 = RpcContext.getContext().getFuture();
        Future<User>future3=RpcContext.getServerContext().getFuture();
        try{
            user=future1.get();
            user=future2.get();
            user=future3.get();
        }catch (InterruptedException e){
            e.printStackTrace();
        }catch (ExecutionException e){
            e.printStackTrace();
        }
        long end=System.currentTimeMillis();
        log.info("用时：{}",end-start);
        return user;
    }
}
