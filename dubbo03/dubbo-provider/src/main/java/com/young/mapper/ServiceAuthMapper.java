package com.young.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.young.entity.ServiceAuth;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ServiceAuthMapper extends BaseMapper<ServiceAuth> {
}
