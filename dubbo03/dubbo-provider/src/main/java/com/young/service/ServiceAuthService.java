package com.young.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.young.entity.ServiceAuth;
import com.young.mapper.ServiceAuthMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceAuthService {
    @Autowired
    private ServiceAuthMapper serviceAuthMapper;
    public Boolean canVisit(String providerName,String consumerName){
        System.out.println("provider:"+providerName+",consumerName:"+consumerName);
        LambdaQueryWrapper<ServiceAuth>queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(ServiceAuth::getProviderService,providerName)
                .eq(ServiceAuth::getConsumerService,consumerName);
        List<ServiceAuth> serviceAuths = serviceAuthMapper.selectList(queryWrapper);
        System.out.println(serviceAuths.size());
        if (serviceAuths!=null&&serviceAuths.size()>0){
            return true;
        }
        return false;
    }
}
