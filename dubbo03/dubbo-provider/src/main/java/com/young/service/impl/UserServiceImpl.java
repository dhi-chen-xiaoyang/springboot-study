package com.young.service.impl;

import com.young.entity.User;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.UserService;

@Service
@DubboService(version = "1.0.0")
public class UserServiceImpl implements UserService {
    @Override
    public User getUserById() {
        User user=new User();
        user.setUsername("cxy");
        user.setPassword("123456");
        user.setId(1);
        user.setSchool("华南理工大学");
        return user;
    }
}
