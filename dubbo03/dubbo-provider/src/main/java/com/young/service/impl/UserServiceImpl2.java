package com.young.service.impl;

import com.young.entity.User;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;
import service.UserService;

@Service
@DubboService(version = "2.0.0")
public class UserServiceImpl2 implements UserService {
    @Override
    public User getUserById() {
        User user=new User();
        user.setUsername("dhi");
        user.setPassword("123456");
        user.setId(1);
        user.setSchool("潮阳一中");
        return user;
    }
}
