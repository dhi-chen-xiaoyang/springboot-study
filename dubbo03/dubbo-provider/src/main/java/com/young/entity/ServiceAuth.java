package com.young.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName(value = "m_service_auth")
public class ServiceAuth implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String providerService;
    private String consumerService;
    private LocalDateTime createTime;
    @TableLogic
    private Integer isDelete;
}
