package service;

import com.young.entity.User;

public interface UserService {
    User getUserById();
}
