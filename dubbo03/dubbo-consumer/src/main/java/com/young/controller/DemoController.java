package com.young.controller;

import com.young.entity.User;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.UserService;

import java.util.UUID;

@RestController
@RequestMapping("/demo")
public class DemoController {
    @DubboReference(version = "1.0.0")
    private UserService userService1;
    @DubboReference(version = "2.0.0")
    private UserService userService2;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("/v1")
    public User getUser1(){
        return userService1.getUserById();
    }

    @GetMapping("/v2")
    public User getUser2(){
        return userService2.getUserById();
    }

    @GetMapping("/token/yes")
    public User hasToken(){
        String token= UUID.randomUUID().toString();
        stringRedisTemplate.opsForValue().set("authorization",token);
        System.out.println("token:"+token);
        RpcContext.getClientAttachment().setAttachment("authorization",token);
        RpcContext.getClientAttachment().setAttachment("name","cxy");
        User user = userService1.getUserById();
        return user;
    }

    @GetMapping("/token/no")
    public User notToken(){
        User user = userService1.getUserById();
        return user;
    }

    @GetMapping("/name")
    public String name(){
        String interfaceName = RpcContext.getServerContext().getInterfaceName();
        String methodName = RpcContext.getServerContext().getMethodName();
        String localHostName = RpcContext.getServerContext().getLocalHostName();
        String remoteApplicationName = RpcContext.getServerContext().getRemoteApplicationName();
        String remoteHostName = RpcContext.getServerContext().getRemoteHostName();
        System.out.println("interface:"+interfaceName);
        System.out.println("method:"+methodName);
        System.out.println("localHost"+localHostName);
        System.out.println("remoteHost:"+remoteHostName);
        System.out.println("remoteApplication:"+remoteApplicationName);
        return remoteApplicationName;
    }
}
