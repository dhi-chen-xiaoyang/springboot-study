package com.young.application.facade;

import com.young.api.sdo.CashierSubmitRequestSDO;
import com.young.api.sdo.CashierSubmitResponseSDO;

public interface CashierService {
    CashierSubmitResponseSDO submit(CashierSubmitRequestSDO cashierSubmitRequestSDO);
}
