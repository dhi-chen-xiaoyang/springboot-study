package com.young.application.convertor;

import com.alibaba.fastjson.JSONObject;
import com.young.domain.entity.CashierModel;
import com.young.infrastructure.po.Order;
import com.young.domain.valueobject.*;
import com.young.infrastructure.dto.AddressDTO;
import com.young.infrastructure.dto.PayerDTO;
import com.young.infrastructure.dto.ProductDTO;
import com.young.api.request.CashierSubmitRequest;
import com.young.api.response.CashierSubmitResponse;
import com.young.api.sdo.CashierSubmitRequestSDO;
import com.young.api.sdo.CashierSubmitResponseSDO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
public interface CashierConverter {
    public CashierSubmitRequestSDO cashierSubmitRequest2SDO(CashierSubmitRequest cashierSubmitRequest);

    public CashierSubmitResponse sdo2CashierSubmitResponse(CashierSubmitResponseSDO cashierSubmitResponseSDO);

    default CashierModel cashierSubmitRequestSDO2Model(CashierSubmitRequestSDO cashierSubmitRequestSDO){
        AddressDTO addressDTO = cashierSubmitRequestSDO.getAddressDTO();
        ProductDTO productDTO = cashierSubmitRequestSDO.getProductDTO();
        PayerDTO payerDTO = cashierSubmitRequestSDO.getPayerDTO();
        Address address=new Address(
                addressDTO.getCountry(),
                addressDTO.getProvince(),
                addressDTO.getCity(),
                addressDTO.getRegion(),
                addressDTO.getTown(),
                addressDTO.getVillage(),
                addressDTO.getDetail()
        );
        Product product=new Product(
                productDTO.getProductNo(),
                productDTO.getProductName(),
                productDTO.getDescription(),
                productDTO.getProductIcon(),
                productDTO.getPrice()
        );
        Payer payer=new Payer(payerDTO.getPayerId(),payerDTO.getPayerName());
        PhoneNumber phoneNumber=new PhoneNumber(cashierSubmitRequestSDO.getPhoneNumber());
        return new CashierModel(address,phoneNumber,product,payer);
    }


    default Order model2OrderDo(CashierModel cashierModel){
        Order order=new Order();
        order.setOrderNo(cashierModel.getOrderNo());
        order.setProductNo(cashierModel.getProduct().getProductNo());
        order.setProductName(cashierModel.getProduct().getProductName());
        order.setProductIcon(cashierModel.getProduct().getProductIcon());
        order.setAddress(JSONObject.toJSONString(cashierModel.getAddress()));
        order.setPayer(JSONObject.toJSONString(cashierModel.getPayer()));
        order.setPrice(cashierModel.getMoney().getAmount());
        order.setOrderStatus(cashierModel.getPayStatus().getPayStatus());
        order.setRiskInfo(JSONObject.toJSONString(cashierModel.getRiskData()));
        order.setCreateTime(System.currentTimeMillis());
        return order;
    }

    default CashierSubmitResponseSDO cashierModel2CashierSubmitResponseSDO(CashierModel cashierModel){
        CashierSubmitResponseSDO cashierSubmitResponseSDO=new CashierSubmitResponseSDO();
        Address address = cashierModel.getAddress();
        PhoneNumber phoneNumber = cashierModel.getPhoneNumber();
        Payer payer = cashierModel.getPayer();
        Product product = cashierModel.getProduct();
        RiskData riskData = cashierModel.getRiskData();
        cashierSubmitResponseSDO.setAddressDTO(address2DTO(address));
        cashierSubmitResponseSDO.setPhoneNumber(phoneNumber.getNumber());
        cashierSubmitResponseSDO.setPayerDTO(payer2DTO(payer));
        cashierSubmitResponseSDO.setAmount(cashierModel.getMoney().getAmount());
        cashierSubmitResponseSDO.setPayStatus(cashierModel.getPayStatus().getPayStatus());
        cashierSubmitResponseSDO.setRiskStatus(riskData.getRiskStatus());
        cashierSubmitResponseSDO.setChallengeType(riskData.getChallengeType());
        cashierSubmitResponseSDO.setProductDTO(product2DTO(product));
        cashierSubmitResponseSDO.setOrderNo(cashierModel.getOrderNo());
        cashierSubmitResponseSDO.setExternalNo(cashierModel.getExternalNo());
        cashierSubmitResponseSDO.setCode(200);
        cashierSubmitResponseSDO.setMsg("操作成功");
        return cashierSubmitResponseSDO;
    }

    ProductDTO product2DTO(Product product);

    PayerDTO payer2DTO(Payer payer);

    AddressDTO address2DTO(Address address);

}
