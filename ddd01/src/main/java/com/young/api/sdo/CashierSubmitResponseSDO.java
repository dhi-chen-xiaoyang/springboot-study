package com.young.api.sdo;

import com.young.infrastructure.dto.AddressDTO;
import com.young.infrastructure.dto.PayerDTO;
import com.young.infrastructure.dto.ProductDTO;
import lombok.Data;

@Data
public class CashierSubmitResponseSDO {
    private Integer code;
    private String msg;
    private String externalNo;
    private String orderNo;
    private ProductDTO productDTO;
    private AddressDTO addressDTO;
    private PayerDTO payerDTO;
    private String phoneNumber;
    private Long amount;
    private String payStatus;
    private String riskStatus;
    private String challengeType;

    public Boolean isFail(){
        return this.code!=200;
    }
}
