package com.young.api.request;

import com.young.infrastructure.dto.AddressDTO;
import com.young.infrastructure.dto.PayerDTO;
import com.young.infrastructure.dto.ProductDTO;
import lombok.Data;

@Data
public class CashierSubmitRequest {
    private String externalNo;
    private String orderNo;
    private ProductDTO productDTO;
    private AddressDTO addressDTO;
    private String phoneNumber;
    private PayerDTO payerDTO;
}
