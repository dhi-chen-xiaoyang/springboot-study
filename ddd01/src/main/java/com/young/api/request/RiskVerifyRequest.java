package com.young.api.request;

import lombok.Data;

@Data
public class RiskVerifyRequest {
    private String code;
    private String payerId;
    private String challengeType;
}
