package com.young.api.response;

import com.young.domain.valueobject.RiskData;
import lombok.Data;

@Data
public abstract class RiskResponse {
    protected Integer code;
    protected String msg;
   protected RiskData riskData=new RiskData();

   public Integer getCode(){
       return this.code;
   }

   public void setCode(Integer code){
       this.code=code;
   }

   public String getMsg(){
       return this.msg;
   }

   public void setMsg(String msg){
       this.msg=msg;
   }

   public RiskData getRiskData(){
       return this.riskData;
   }

   public void setRiskData(RiskData riskData){
       this.riskData=riskData;
   }

    public Boolean isFail(){
        return this.code!=200;
    }
}
