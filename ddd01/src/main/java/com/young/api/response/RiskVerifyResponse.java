package com.young.api.response;

import lombok.Data;

@Data
public class RiskVerifyResponse extends RiskResponse {
    private Boolean verifyResult;
}
