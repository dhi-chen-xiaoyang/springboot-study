package com.young.api.response;

import com.young.infrastructure.dto.AddressDTO;
import com.young.infrastructure.dto.ProductDTO;
import lombok.Data;

@Data
public class CashierSubmitResponse {
    private String externalNo;
    private String orderNo;
    private ProductDTO productDTO;
    private AddressDTO addressDTO;
    private String phoneNumber;
    private Long amount;
    private String payStatus;
    private String riskStatus;
    private String challengeType;
}
