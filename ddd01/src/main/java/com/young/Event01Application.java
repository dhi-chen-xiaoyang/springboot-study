package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Event01Application {
    public static void main(String[] args) {
        SpringApplication.run(Event01Application.class,args);
    }
}
