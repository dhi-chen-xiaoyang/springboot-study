package com.young.infrastructure.client;

import com.young.api.response.RiskConsultResponse;
import com.young.api.response.RiskVerifyResponse;

public interface RiskService {
    RiskConsultResponse consultRisk();
    RiskVerifyResponse verifyRisk();
}
