package com.young.infrastructure.client.impl;

import com.young.infrastructure.client.RiskService;
import com.young.infrastructure.common.enums.RiskStatusEnum;
import com.young.api.response.RiskConsultResponse;
import com.young.api.response.RiskVerifyResponse;
import org.springframework.stereotype.Service;

/**
 * 模拟远程调用
 */
@Service
public class DubboRiskService implements RiskService {
    @Override
    public RiskConsultResponse consultRisk() {
        RiskConsultResponse response=new RiskConsultResponse();
        response.getRiskData().setRiskStatus(RiskStatusEnum.PASS.name());
        response.getRiskData().setChallengeType("OTP");
        response.setCode(200);
        response.setMsg("操作成功");
        return response;
    }

    @Override
    public RiskVerifyResponse verifyRisk() {
        return null;
    }
}
