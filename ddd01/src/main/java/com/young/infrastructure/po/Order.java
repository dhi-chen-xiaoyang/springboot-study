package com.young.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "t_order")
public class Order implements Serializable {
    @TableId(type = IdType.INPUT)
    private String orderNo;
    private String productNo;
    private String productName;
    private String productIcon;
    private String address;
    private String payer;
    @TableField(value = "risk")
    private String riskInfo;
    private Long price;
    private String orderStatus;
    private Long createTime;
    private Long paidTime;
}
