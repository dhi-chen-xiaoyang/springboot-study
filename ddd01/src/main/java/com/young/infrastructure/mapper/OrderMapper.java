package com.young.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.young.infrastructure.po.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
    @Insert("insert into order(order_no,product_no,product_name,product_icon,address,payer,risk,price,order_status,create_time,paid_time)" +
            "values (#orderNo}," +
            "#{productNo}," +
            "#{productName}," +
            "#{productIcon}," +
            "#{address}," +
            "#{payer}," +
            "#{riskInfo}," +
            "#{price}," +
            "#{orderStatus}," +
            "#{createTime}," +
            "#{paidTime})")
    Integer insertOrder(Order item);
}
