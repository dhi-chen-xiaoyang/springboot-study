package com.young.infrastructure.common.enums;

public enum RiskStatusEnum {
    PASS,
    NEED_VERIFY,
    REJECT;
}
