package com.young.infrastructure.common.vo;

import lombok.Data;

@Data
public class Result <T>{
    private Integer code;
    private String msg;
    private T data;

    private Result(Integer code,String msg,T data){
        this.code=code;
        this.msg=msg;
        this.data=data;
    }

    private Result(Integer code,String msg){
        this.code=code;
        this.msg=msg;
        this.data=null;
    }

    public static <T> Result<T> success(T data){
        return new Result<>(200,"操作成功",data);
    }

    public static <T> Result<T> success(){
        return new Result<>(200,"操作成功");
    }

    public static <T> Result<T> fail(String msg){
        return new Result<>(400,msg);
    }

    public static <T> Result<T> fail(Integer code,String msg){
        return new Result<>(code,msg);
    }

    public static <T> Result<T> fail(){
        return new Result<>(400,"操作失败");
    }
}
