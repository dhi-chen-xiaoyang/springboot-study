package com.young.infrastructure.common.enums;

public enum PayStatusEnum {
    INIT,
    PAYING,
    PAID,
    FAIL;
}
