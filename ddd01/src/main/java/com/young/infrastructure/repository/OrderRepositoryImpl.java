package com.young.infrastructure.repository;

import com.young.domain.repository.OrderRepository;
import com.young.infrastructure.mapper.OrderMapper;
import com.young.infrastructure.po.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class OrderRepositoryImpl implements OrderRepository {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Boolean save(Order order) {
        order.setCreateTime(System.currentTimeMillis());
        System.out.println(order);
        return orderMapper.insert(order) > 0;
    }

    @Override
    public Boolean updateOrder(Order order) {
        order.setPaidTime(System.currentTimeMillis());
        return orderMapper.updateById(order)>0;
    }
}
