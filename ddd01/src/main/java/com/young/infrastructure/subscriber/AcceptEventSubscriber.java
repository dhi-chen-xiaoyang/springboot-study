package com.young.infrastructure.subscriber;

import com.young.domain.entity.CashierModel;
import com.young.domain.valueobject.PayStatus;
import com.young.infrastructure.common.enums.PayStatusEnum;
import com.young.infrastructure.event.AcceptEvent;
import com.young.infrastructure.event.PaidEvent;
import com.young.infrastructure.event.RejectEvent;
import com.young.infrastructure.publisher.PaidEventPublisher;
import com.young.infrastructure.publisher.RejectEventPublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AcceptEventSubscriber {
    @Autowired
    private PaidEventPublisher paidEventPublisher;
    @Autowired
    private RejectEventPublisher rejectEventPublisher;

    @EventListener(classes = AcceptEvent.class)
    public void handler(AcceptEvent acceptEvent){
        CashierModel cashierModel = acceptEvent.getCashierModel();
        log.info("cashierModel:{}",cashierModel);
        if (cashierModel.passRisk()) {
            log.info("通过风控================");
            PayStatus payStatus=new PayStatus();
            payStatus.setPayStatus(PayStatusEnum.PAID.name());
            //更新支付单状态
            cashierModel.updatePayStatus(payStatus);
            PaidEvent paidEvent=new PaidEvent(cashierModel);
            paidEventPublisher.publish(paidEvent);
        }else{
            log.info("风控不通过=============");
            PayStatus payStatus=new PayStatus();
            payStatus.setPayStatus(PayStatusEnum.FAIL.name());
            //更新支付单状态
            cashierModel.updatePayStatus(payStatus);
            RejectEvent rejectEvent=new RejectEvent(cashierModel);
            rejectEventPublisher.publish(rejectEvent);
        }
    }
}
