package com.young.infrastructure.subscriber;

import com.young.application.convertor.CashierConverter;
import com.young.domain.repository.OrderRepository;
import com.young.infrastructure.event.AcceptEvent;
import com.young.infrastructure.event.PaidEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PaidEventSubscriber {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CashierConverter cashierConverter;

    @EventListener(classes = PaidEvent.class)
    public void handler(PaidEvent paidEvent){
        log.info("cashierModel:{}",paidEvent.getCashierModel());
        log.info("支付成功==============");
        log.info("更新order==============");
        orderRepository.updateOrder(cashierConverter.model2OrderDo(paidEvent.getCashierModel()));
    }
}
