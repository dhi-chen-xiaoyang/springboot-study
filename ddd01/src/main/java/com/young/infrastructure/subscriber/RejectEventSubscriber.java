package com.young.infrastructure.subscriber;

import com.young.application.convertor.CashierConverter;
import com.young.domain.repository.OrderRepository;
import com.young.infrastructure.event.AcceptEvent;
import com.young.infrastructure.event.PaidEvent;
import com.young.infrastructure.event.RejectEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RejectEventSubscriber {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CashierConverter cashierConverter;

    @EventListener(classes = RejectEvent.class)
    public void handler(RejectEvent rejectEvent){
        log.info("cashierModel:{}",rejectEvent.getCashierModel());
        log.info("支付失败==============");
        log.info("更新order==============");
        orderRepository.updateOrder(cashierConverter.model2OrderDo(rejectEvent.getCashierModel()));
    }
}
