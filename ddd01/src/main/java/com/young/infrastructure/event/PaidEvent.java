package com.young.infrastructure.event;

import com.young.domain.entity.CashierModel;
import org.springframework.context.ApplicationEvent;

public class PaidEvent extends ApplicationEvent {
    private CashierModel cashierModel;
    public PaidEvent(CashierModel cashierModel) {
        super(new Object());
        this.cashierModel=cashierModel;
    }

    public CashierModel getCashierModel() {
        return cashierModel;
    }
}
