package com.young.infrastructure.event;

import com.young.domain.entity.CashierModel;
import org.springframework.context.ApplicationEvent;

public class AcceptEvent extends ApplicationEvent {
    private CashierModel cashierModel;
    public AcceptEvent(CashierModel cashierModel) {
        super(new Object());
        this.cashierModel=cashierModel;
    }

    public CashierModel getCashierModel() {
        return cashierModel;
    }

}
