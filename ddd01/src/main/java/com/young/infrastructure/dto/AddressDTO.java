package com.young.infrastructure.dto;

import lombok.Data;

@Data
public class AddressDTO {
    private String country;
    private String province;
    private String city;
    private String region;
    private String town;
    private String village;
    private String detail;
}
