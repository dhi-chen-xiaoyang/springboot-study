package com.young.infrastructure.dto;

import lombok.Data;

@Data
public class ProductDTO {
    private String productNo;
    private String productName;
    private String description;
    private String productIcon;
    private Long price;
}
