package com.young.infrastructure.dto;

import lombok.Data;

@Data
public class PayerDTO {
    private String payerId;
    private String payerName;
}
