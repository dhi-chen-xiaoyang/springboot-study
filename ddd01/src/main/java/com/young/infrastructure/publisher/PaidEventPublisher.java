package com.young.infrastructure.publisher;

import com.young.infrastructure.event.PaidEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class PaidEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publish(PaidEvent data){
        applicationEventPublisher.publishEvent(data);
    }
}
