package com.young.infrastructure.publisher;

import com.young.infrastructure.event.AcceptEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class AcceptEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publish(AcceptEvent data){
        applicationEventPublisher.publishEvent(data);
    }
}
