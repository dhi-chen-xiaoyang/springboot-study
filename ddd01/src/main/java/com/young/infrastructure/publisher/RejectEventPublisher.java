package com.young.infrastructure.publisher;

import com.young.infrastructure.event.RejectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class RejectEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publish(RejectEvent data){
        applicationEventPublisher.publishEvent(data);
    }
}
