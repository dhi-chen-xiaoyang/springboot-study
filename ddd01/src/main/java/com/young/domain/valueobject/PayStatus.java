package com.young.domain.valueobject;

import com.young.infrastructure.common.enums.PayStatusEnum;
import lombok.Data;

@Data
public class PayStatus {
    private String payStatus;
    public PayStatus(){
        this.payStatus= PayStatusEnum.INIT.name();
    }

    public boolean isPaid() {
        return PayStatusEnum.PAID.name().equals(this.payStatus);
    }

    public Boolean isProcessing() {
        return PayStatusEnum.PAYING.name().equals(this.payStatus);
    }
}
