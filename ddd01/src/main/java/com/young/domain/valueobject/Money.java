package com.young.domain.valueobject;

import lombok.Data;

import javax.xml.bind.ValidationException;

@Data
public class Money {
    private final String defaultCurrency="CNY";
    //金额
    private Long amount;
    //货币类型
    private String currency;

    public Money(Long amount){
        if (amount<0){
            throw new IllegalArgumentException("金额不能为负数");
        }
        this.amount=amount;
        this.currency=defaultCurrency;
    }

    public Money(Long amount,String currency){
        if (amount<0){
            throw new IllegalArgumentException("金额不能为负数");
        }
        if (currency == null || "".equals(currency)) {
            throw new IllegalArgumentException("货币类型非法");
        }
        this.amount=amount;
        this.currency=currency;
    }
}
