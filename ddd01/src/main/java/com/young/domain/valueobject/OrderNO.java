package com.young.domain.valueobject;

import lombok.Data;

@Data
public class OrderNO {
    private String orderNo;
    private String externalRefNo;

    public OrderNO(String orderNo,String externalRefNo){
        this.orderNo=orderNo;
        this.externalRefNo=externalRefNo;
    }

    public OrderNO(){}
}
