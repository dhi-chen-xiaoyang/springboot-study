package com.young.domain.valueobject;

import lombok.Data;

import java.util.Objects;

@Data
public class Product {
    private String productNo;
    private String productName;
    private String description;
    private String productIcon;
    private Long price;

    public Product(String productNo,String name,String description,String icon,Long price){
        if (Objects.isNull(productNo)||"".equals(productNo)){
            throw new IllegalArgumentException("商品id不合法");
        }
        if (Objects.isNull(name) || "".equals(name)) {
            throw new IllegalArgumentException("商品名称不能为空");
        }
        if (Objects.isNull(price)||price<0){
            throw new IllegalArgumentException("商品价格非法");
        }
        this.productNo=productNo;
        this.productName=name;
        this.description=description;
        this.productIcon=icon;
        this.price=price;
    }
}
