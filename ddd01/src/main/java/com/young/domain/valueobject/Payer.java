package com.young.domain.valueobject;

import lombok.Data;

@Data
public class Payer {
    private String payerId;
    private String payerName;

    public Payer(String payerId,String payerName){
        if (payerId == null || "".equals(payerId)) {
            throw new IllegalArgumentException("payer不合法");
        }
        this.payerId=payerId;
        this.payerName=payerName;
    }
}
