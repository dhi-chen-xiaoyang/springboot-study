package com.young.domain.valueobject;

import com.young.infrastructure.common.enums.RiskStatusEnum;
import lombok.Data;

@Data
public class RiskData {
    private String riskStatus;
    private String challengeType;
    public RiskData(){
        this.riskStatus= RiskStatusEnum.PASS.name();
    }

    public Boolean needVerify() {
        return RiskStatusEnum.NEED_VERIFY.name().equals(this.riskStatus);
    }

    public Boolean isPass() {
        return RiskStatusEnum.PASS.name().equals(this.riskStatus);
    }
}
