package com.young.domain.valueobject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private String country;
    private String province;
    private String city;
    private String region;
    private String town;
    private String village;
    private String detail;
}
