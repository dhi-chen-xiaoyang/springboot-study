package com.young.domain.valueobject;

import lombok.Data;
import lombok.Getter;

import javax.xml.bind.ValidationException;

@Data
public class PhoneNumber {
    private final String number;
    private final String pattern="^1[3-9]\\d{9}$";
    public String getNumber(){
        return number;
    }
    //仅存在含参数构造器
    public PhoneNumber(String number)  {
        if (number==null){
            throw new IllegalArgumentException("number不能为空");
        }else if(!isValid(number)){
            throw new IllegalArgumentException("number格式错误");
        }
        this.number=number;
    }

    private boolean isValid(String number){
        return number.matches(pattern);
    }
}
