package com.young.domain.entity;

import com.young.domain.valueobject.*;
import lombok.Getter;

import java.util.UUID;

/**
 * 收银意向
 */
@Getter
public class CashierModel {
    private OrderNO orderNO;
    private Address address;
    private PhoneNumber phoneNumber;
    private Product product;
    private RiskData riskData;
    private Money money;
    private PayStatus payStatus;
    private Payer payer;

    public CashierModel(Address address, PhoneNumber phoneNumber, Product product,Payer payer){
        this.address=address;
        this.phoneNumber=phoneNumber;
        this.product=product;
        this.payStatus=new PayStatus();
        this.riskData=new RiskData();
        this.payer=payer;
        enrichMoney();
    }

    public void updateRiskStatus(RiskData riskData){
        if (this.riskData==null){
            this.riskData=riskData;
            return;
        }
        this.riskData.setRiskStatus(riskData.getRiskStatus());
    }

    public void updatePayStatus(PayStatus payStatus){
        if (this.payStatus==null){
            this.payStatus=payStatus;
            return;
        }
        this.payStatus.setPayStatus(payStatus.getPayStatus());
    }

    private void enrichMoney(){
        Long price = this.product.getPrice();
        this.money=new Money(price);
    }

    public Boolean isPaid(){
        if (this.payStatus!=null){
            return this.payStatus.isPaid();
        }
        return false;
    }

    public Boolean isProcessing(){
        if (this.payStatus!=null){
            return this.payStatus.isProcessing();
        }
        return false;
    }

    public Boolean needVerify(){
        if (this.riskData!=null){
            return this.riskData.needVerify();
        }
        return false;
    }

    public Boolean passRisk(){
        if (this.riskData!=null){
            return this.riskData.isPass();
        }
        return false;
    }

    public void initOrderNo(String orderNo, String externalNo) {
        if (orderNo==null&&externalNo==null){
            orderNo= UUID.randomUUID().toString().replace("-","");
        }
        this.orderNO=new OrderNO(orderNo,externalNo);
    }

    public String getOrderNo() {
        if (orderNO.getOrderNo()!=null){
            return orderNO.getOrderNo();
        }
        return orderNO.getExternalRefNo();
    }

    public String getExternalNo() {
        return orderNO.getExternalRefNo();
    }
}
