package com.young.domain.repository;


import com.young.infrastructure.po.Order;

public interface OrderRepository {
    Boolean save(Order order);

    Boolean updateOrder(Order order);
}
