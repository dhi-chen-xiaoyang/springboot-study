package com.young.facade.impl;

import com.young.application.facade.CashierService;
import com.young.facade.CashierFacade;
import com.young.infrastructure.common.vo.Result;
import com.young.application.convertor.CashierConverter;
import com.young.api.request.CashierSubmitRequest;
import com.young.api.response.CashierSubmitResponse;
import com.young.api.sdo.CashierSubmitRequestSDO;
import com.young.api.sdo.CashierSubmitResponseSDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CashierFacadeImpl implements CashierFacade {
    @Autowired
    private CashierService cashierService;
    @Autowired
    private CashierConverter cashierConverter;

    @Override
    public Result<CashierSubmitResponse> submitCashier(CashierSubmitRequest cashierSubmitRequest) {
        CashierSubmitRequestSDO cashierSubmitRequestSDO = cashierConverter.cashierSubmitRequest2SDO(cashierSubmitRequest);
        CashierSubmitResponseSDO responseSDO = cashierService.submit(cashierSubmitRequestSDO);
        if (responseSDO==null){
            return Result.fail();
        }

        if (responseSDO.isFail()){
            return Result.fail(responseSDO.getCode(),responseSDO.getMsg());
        }

        CashierSubmitResponse cashierSubmitResponse = cashierConverter.sdo2CashierSubmitResponse(responseSDO);
        return Result.success(cashierSubmitResponse);
    }
}
