package com.young.facade;

import com.young.infrastructure.common.vo.Result;
import com.young.api.request.CashierSubmitRequest;
import com.young.api.response.CashierSubmitResponse;

public interface CashierFacade {

    Result<CashierSubmitResponse> submitCashier(CashierSubmitRequest cashierSubmitRequest);
}
