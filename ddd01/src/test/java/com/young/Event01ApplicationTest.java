package com.young;

import com.young.api.request.CashierSubmitRequest;
import com.young.api.response.CashierSubmitResponse;
import com.young.domain.valueobject.Product;
import com.young.facade.CashierFacade;
import com.young.infrastructure.common.vo.Result;
import com.young.infrastructure.dto.AddressDTO;
import com.young.infrastructure.dto.PayerDTO;
import com.young.infrastructure.dto.ProductDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
@Slf4j
public class Event01ApplicationTest {
    @Autowired
    private CashierFacade cashierFacade;

    @Test
    public void testSubmitCashier(){
        log.info("testSubmitCashier=====================");
        CashierSubmitRequest cashierSubmitRequest=new CashierSubmitRequest();
        cashierSubmitRequest.setProductDTO(newProductDTO());
        cashierSubmitRequest.setAddressDTO(newAddressDTO());
        cashierSubmitRequest.setPayerDTO(newPayerDTO());
        cashierSubmitRequest.setPhoneNumber("13400008118");
        Result<CashierSubmitResponse> cashierSubmitResponseResult = cashierFacade.submitCashier(cashierSubmitRequest);
        System.out.println(cashierSubmitResponseResult);
    }

    private PayerDTO newPayerDTO() {
        PayerDTO payerDTO=new PayerDTO();
        payerDTO.setPayerId("100");
        payerDTO.setPayerName("DHI");
        return payerDTO;
    }

    private AddressDTO newAddressDTO() {
        AddressDTO addressDTO=new AddressDTO();
        addressDTO.setCountry("China");
        addressDTO.setProvince("ZheJiang");
        addressDTO.setCity("YiWu");
        addressDTO.setRegion("YiWu");
        addressDTO.setTown("YiWu");
        addressDTO.setVillage("YiWu");
        addressDTO.setDetail("浙江义乌小商品");
        return addressDTO;
    }

    private ProductDTO newProductDTO() {
       ProductDTO productDTO=new ProductDTO();
       productDTO.setProductNo(UUID.randomUUID().toString().replace("-",""));
       productDTO.setProductName("哈哈哈哈哈哈哈");
       productDTO.setDescription("描述");
       productDTO.setProductIcon("http://image");
       productDTO.setPrice(100L);
       return productDTO;
    }
}
