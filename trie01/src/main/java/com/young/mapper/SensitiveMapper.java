package com.young.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.young.entity.Sensitive;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SensitiveMapper extends BaseMapper<Sensitive> {
}
