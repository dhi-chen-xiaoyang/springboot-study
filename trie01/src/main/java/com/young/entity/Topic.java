package com.young.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@TableName(value = "m_topic")
@Data
public class Topic {
    @TableId("id")
    private String id;
    private String title;
    private String content;
    private String userId;
    private LocalDateTime createTime;
    @TableLogic
    private Integer isDelete;
}
