package com.young.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@TableName(value = "m_user")
@Data
public class User {
    @TableId("id")
    private String id;
    private String username;
    private String password;
    private LocalDateTime createTime;
    private Integer isEnabled;
    @TableLogic
    private Integer isDelete;
}
