package com.young.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@TableName(value = "m_sensitive")
@Data
public class Sensitive {
    @TableId("id")
    private String id;
    private String word;
    private LocalDateTime createTime;
    @TableLogic
    private Integer isDelete;
}
