package com.young.util;

import com.young.vo.ResultVO;

public class ResultVOUtil <T>{
    public static <T> ResultVO<T> success(){
        return new ResultVO<>(200,"操作成功");
    }
    public static <T> ResultVO<T> success(T data){
        return new ResultVO<>(200,"操作成功",data);
    }
    public static <T> ResultVO<T> fail(){
        return new ResultVO<>(400,"操作失败");
    }
    public static <T> ResultVO<T> fail(Integer code,String msg){
        return new ResultVO<>(code,msg);
    }
}
