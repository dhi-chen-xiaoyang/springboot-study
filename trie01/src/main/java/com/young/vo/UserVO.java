package com.young.vo;

import lombok.Data;

@Data
public class UserVO {
    private String username;
    private String password;
}
