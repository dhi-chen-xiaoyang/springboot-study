package com.young.vo;

import java.util.HashMap;
import java.util.Map;

public class TrieTree01 {
    public class TrieNode{
        public char word;
        public int isEnd;
        public Map<Character,TrieNode>children;
        public TrieNode(){
            this.isEnd=0;
            this.children=new HashMap<>();
        }
        public TrieNode(char word){
            this.word=word;
            this.isEnd=0;
            this.children=new HashMap<>();
        }
    }
    private TrieNode root;
    public TrieTree01(){
        this.root=new TrieNode();
    }
    public void insert(String str){
        if (str == null || "".equals(str)) {
            return;
        }
        root=insert(root,str,0);
    }
    public boolean match(String str){
        if (str == null || "".equals(str)) {
            return false;
        }
        TrieNode temp=root;
        for(int i=0;i<str.length();i++){
            char ch=str.charAt(i);
            TrieNode next = temp.children.get(ch);
            if (next==null){
                temp=root;
            }else{
                temp=next;
            }
            if (temp.isEnd==1){
                return true;
            }
        }
        return false;
    }
    public void remove(String str){
        if (!match(str)) {
            return ;
        }
        remove(root,str,0);
    }
    private TrieNode remove(TrieNode t,String str,int index){
        char ch=str.charAt(index);
        TrieNode child = t.children.get(ch);
        if (child==null){
            return t;
        }
        if(index==str.length()-1){
            if (child.children.size()>0){
                child.isEnd=0;
            }else{
                t.children.remove(ch);
            }
            return t;
        }
        child=remove(child,str,index+1);
        if (child.children.size()>0){
            child.isEnd=0;
        }else{
            t.children.remove(ch);
        }
        return t;
    }
    private TrieNode insert(TrieNode t,String str,int index){
        char ch=str.charAt(index);
        TrieNode child = t.children.get(ch);
        if (child!=null){
            if (index==str.length()-1){
                //当到达最后一个时
                child.isEnd=1;
                return t;
            }
            child=insert(child,str,index+1);
            return t;
        }
        child=new TrieNode(ch);
        if (index==str.length()-1){
            //当到达最后一个时
            child.isEnd=1;
        }else{
            child=insert(child,str,index+1);
        }
        t.children.put(ch,child);
        return t;
    }

    public static void main(String[] args) {
        String[]sensitive={"华南理工","大学生","泰裤辣"};
        TrieTree trieTree=new TrieTree();
        for(int i=0;i<sensitive.length;i++){
            trieTree.insert(sensitive[i]);
        }
        System.out.println(trieTree.match("我是华南大学的学生"));
        System.out.println(trieTree.match("华北理工大学泰裤"));
        System.out.println(trieTree.match("华南理工大学"));
        System.out.println(trieTree.match("大学生"));
        System.out.println(trieTree.match("大学生泰裤辣"));
        System.out.println(trieTree.match("人之初性本善性相近习相远华南大学泰山崩于前而面不改色泰裤辣哈哈哈哈哈哈"));
        trieTree.remove("华南理工");
        System.out.println(trieTree.match("华南理工大学"));
        trieTree.remove("大学生");
        System.out.println(trieTree.match("大学生"));
        trieTree.remove("泰裤辣");
        System.out.println(trieTree.match("人之初性本善性相近习相远华南大学泰山崩于前而面不改色泰裤辣哈哈哈哈哈哈"));
    }
}
