package com.young.exception;

public class BusinessException extends RuntimeException{
    private String msg;
    public BusinessException(String msg){
        super(msg);
    }
}
