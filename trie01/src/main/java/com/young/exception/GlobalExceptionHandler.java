package com.young.exception;

import com.young.util.ResultVOUtil;
import com.young.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(BusinessException.class)
    public ResultVO businessExceptionHandler(BusinessException e){
        log.error("businessException:{}",e);
        return ResultVOUtil.fail(400,e.getMessage());
    }
    @ExceptionHandler(Exception.class)
    public ResultVO exceptionHandler(Exception e){
        log.error("exception:{}",e);
        return ResultVOUtil.fail(400,e.getMessage());
    }
}
