package com.young.config;

import com.young.service.SensitiveService;
import com.young.vo.TrieTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class TrieTreeConfig {
    @Autowired
    private SensitiveService sensitiveService;
    @Bean
    public TrieTree constructTrieTree(){
        System.out.println("初始化字典树======================");
        List<String> words = sensitiveService.getAllSensitiveWord();
        TrieTree trieTree=new TrieTree();
        for (String word : words) {
            trieTree.insert(word);
        }
        return trieTree;
    }
}
