package com.young.service;

import com.young.entity.Sensitive;
import com.young.mapper.SensitiveMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SensitiveService {
    @Autowired
    private SensitiveMapper sensitiveMapper;
    public List<Sensitive>getAllSensitive(){
        return sensitiveMapper.selectList(null);
    }
    public List<String>getAllSensitiveWord(){
        List<Sensitive> allSensitive = getAllSensitive();
        if (allSensitive!=null&&allSensitive.size()>0){
            return allSensitive.stream().map(sensitive -> sensitive.getWord()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
