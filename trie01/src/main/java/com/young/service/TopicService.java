package com.young.service;

import com.young.entity.Topic;
import com.young.exception.BusinessException;
import com.young.mapper.TopicMapper;
import com.young.vo.TrieTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TopicService {
    @Autowired
    private TopicMapper topicMapper;
    @Resource
    private TrieTree trieTree;
    public boolean saveTopic(Topic topic){
        //判断是否有敏感词汇
        if (trieTree.match(topic.getTitle())||trieTree.match(topic.getContent())) {
            throw new BusinessException("发布内容中存在不当词汇，请遵守相关法律法规，营造良好的网络环境!!!");
        }
        return topicMapper.insert(topic)>0;
    }
}
