package com.young.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@TableName(value = "m_user")
public class User {
    @TableId
    private Integer id;
    private String username;
    private String password;
    private LocalDateTime loginTime;
    private String role;
}
