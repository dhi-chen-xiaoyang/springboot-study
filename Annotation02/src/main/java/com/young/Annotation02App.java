package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Annotation02App {
    public static void main(String[] args) {
        SpringApplication.run(Annotation02App.class,args);
    }
}
