package com.young.controller;

import com.young.annotation.AdminIntercept;
import com.young.entity.User;
import com.young.service.UserService;
import com.young.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping("/login")
    public String login(@RequestBody UserVO userVO, HttpServletRequest request){
        User user = userService.login(userVO.getUsername(), userVO.getPassword());
        if (user!=null){
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            return "登录成功";
        }
        return "登录失败";
    }
    @GetMapping("/info")
    @AdminIntercept
    public User info(HttpServletRequest request){
        User user = (User)request.getSession().getAttribute("user");
        return user;
    }
}