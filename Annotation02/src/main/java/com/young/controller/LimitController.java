package com.young.controller;

import com.young.annotation.LimitIntercept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/limit")
public class LimitController {
    @GetMapping("/test1")
    @LimitIntercept
    public String test1(){
        return "test1";
    }
    @GetMapping("/test2")
    @LimitIntercept(maxCount = 1,waitTime = 10)
    public String test2(){
        return "test2";
    }
}
