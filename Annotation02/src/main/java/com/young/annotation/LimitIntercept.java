package com.young.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LimitIntercept {
    boolean required()default true;

    //设置默认5秒内最多点击3次

    int maxCount()default  3; //默认最多点击3次
    int waitTime()default 5; //默认时长5秒
}
