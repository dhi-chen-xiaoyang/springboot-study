package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreadPool01App {
    public static void main(String[] args) {
        SpringApplication.run(ThreadPool01App.class,args);
    }
}
