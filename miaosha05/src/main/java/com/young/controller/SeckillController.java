package com.young.controller;

import com.young.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SeckillController {
    private static Integer productId=100;
    private static Integer orderId=101;

    @Autowired
    private OrderService orderService;

    @GetMapping("/seckill")
    public String seckill(){
        return orderService.seckill(productId,orderId);
    }
}
