package com.young.listener;

import com.young.config.DirectRabbitMQConfig;
import com.young.entity.Order;
import com.young.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RabbitListener(queues = DirectRabbitMQConfig.DIRECT_ORDER_QUEUE)
public class DirectConsumer {
    @Autowired
    private OrderService orderService;
    @RabbitHandler
    public void directHandler(Order order) {
        /**
         * 对于秒杀系统中，消息队列在处理订单消息的时候，要考虑到下面两个问题，
         * 第一个是订单幂等性，不能出现重复下单的情况；
         * 第二个是如何解决消息积压的情况，在网课中，它的解决方法是在根据现在取出的订单，查看订单对应的时间戳，如果瞬间超过1分钟，说明我们堆积了1
         * 分钟的消息，那么我们就把当前取到的消息，进行丢弃，而前端在根据订单号查询订单信息时，查询不到内容也就知道秒杀下单失败
         */
        //这里为了方便观察，假设监听时间超过10秒，就进行丢弃
        Long oldTime = order.getVersion();
        if (System.currentTimeMillis()-oldTime<=10000) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (orderService.saveOrder(order)) {
                log.info("插入成功==============");
            }
        }else{
            log.info("丢弃消息==============");
        }

        //上面的代码可以再进行改进，比如使用批量插入来提高效率，如何实现，可以参考threadPool03批量插入日志
    }
}
