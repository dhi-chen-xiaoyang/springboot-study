package com.young.service;

import com.young.entity.Order;

public interface OrderService {
    Boolean saveOrder(Order order);
    String seckill(Integer productId,Integer userId);
}
