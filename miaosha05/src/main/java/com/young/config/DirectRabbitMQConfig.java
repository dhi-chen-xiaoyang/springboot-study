package com.young.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class DirectRabbitMQConfig {
    public final static String DIRECT_ORDER_QUEUE="direct_order_queue";
    public final static String DIRECT_ORDER_EXCHANGE="direct_order_exchange";
    public final static String DIRECT_ORDER_ROUTING="direct_order_routing";

    @Bean
    public Queue directOrderQueue(){
        return new Queue(DIRECT_ORDER_QUEUE,true);
    }

    @Bean
    public DirectExchange directOrderExchange(){
        return new DirectExchange(DIRECT_ORDER_EXCHANGE,true,false);
    }

    @Bean
    public Binding directOrderBinding(){
        return BindingBuilder.bind(directOrderQueue())
                .to(directOrderExchange())
                .with(DIRECT_ORDER_ROUTING);
    }
}
