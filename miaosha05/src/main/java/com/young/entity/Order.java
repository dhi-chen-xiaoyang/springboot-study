package com.young.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "t_order")
public class Order implements Serializable {
    @TableId("id")
    private String id;
    private Integer productId;
    private Integer userId;
    private Long version;
}
