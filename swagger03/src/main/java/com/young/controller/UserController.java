package com.young.controller;

import com.young.entity.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Tag(name = "用户管理")
public class UserController {
    @Operation(summary = "根据用户id获取用户",tags = "用户管理")
    @Parameters({
            @Parameter(name = "id",description = "用户id")
    })
    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id")String id){
        User user=new User();
        user.setId(id);
        user.setName("hello");
        user.setAge("0");
        return user;
    }
}
