package com.young.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http)throws Exception{
        http.authorizeRequests().antMatchers("/static/**",
                "/swagger-resources/**",
                "/documentation/**", //因为刚才在application.yml里面设置baseUrl为documentation，所以要加上这个
                "/v2/**",
                "/v3/**",
                "/swagger-ui/**",
                "**/upload/**",
                "/index.jsp").permitAll()
                .anyRequest().authenticated();
    }
}
