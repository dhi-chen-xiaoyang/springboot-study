package com.young.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class Swagger03Config {
    @Bean
    public Docket createDocket(){
        return new Docket(DocumentationType.OAS_30)
                .enable(true) //是否开启OpenApi
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.young.controller")) //指定要扫描的包
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("未来先遣者")
                .contact(new Contact("沉河不浮",
                        "https://blog.csdn.net/weixin_55850954",
                        "2827523200@qq.com"))
                .description("记录SpringBoot整合Swagger3")
                .license("Apache2.0")
                .licenseUrl("https://localhost:apache")
                .build();
    }
}
