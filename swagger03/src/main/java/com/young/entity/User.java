package com.young.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "用户类")
public class User {
    @Schema(description = "用户id")
    private String id;
    @Schema(description = "用户姓名")
    private String name;
    @Schema(description = "用户年龄")
    private String age;
}
