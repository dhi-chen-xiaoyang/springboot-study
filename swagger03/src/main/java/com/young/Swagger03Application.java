package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication
@EnableOpenApi
public class Swagger03Application {
    public static void main(String[] args) {
        SpringApplication.run(Swagger03Application.class,args);
    }
}
