package com.young.test3;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        A a=new A();
        //实现词库元素的生成
        a.execute(new CallBackInterface() {
            @Override
            public Object process(List param) {
                List<String>wordList=param;
                wordList.remove("1");
                return true;
            }
        });
        a.execute(new CallBackInterface() {
            @Override
            public Object process(List param) {
                List<String>wordList=param;
                wordList.add("24");
                return true;
            }
        });
    }
}
