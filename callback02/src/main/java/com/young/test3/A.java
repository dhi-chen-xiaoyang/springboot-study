package com.young.test3;

import java.util.ArrayList;
import java.util.List;

public class A {
    public List<String>wordList=loadList();
    public <T> T execute(CallBackInterface callBackInterface){
        //可以先执行一些execute的逻辑
        //直接将相关词库的操作全都交给了callbackInterface
        T result=(T)callBackInterface.process(wordList);
        for(String str:wordList){
            System.out.print(str+" ");
        }
        System.out.println();
        return result;
    }
    //加载词库到内存中
    public List<String>loadList(){
        List<String>wordList=new ArrayList<>();
        for(int i=0;i<10;i++){
            wordList.add(Integer.toString(i));
        }
        return wordList;
    }
}
