package com.young.test3;

import java.util.List;

public interface CallBackInterface <T>{
    T process(List<Object>param);
}
