package com.young.test6;

import com.young.vo.ResultVO;

public class CallbackImpl implements CallbackInterface{
    private ResultVO resultVO;
    private Integer res;
    @Override
    public void checkParam(int num) {
        if (num%2==1){
            throw new RuntimeException("参数有误");
        }
    }

    @Override
    public void process(int num) {
        this.res=2*num+1;
    }

    @Override
    public void success() {
        resultVO=new ResultVO(true,"操作成功",res);
    }

    @Override
    public void fail(Exception e) {
        resultVO=new ResultVO(false,e.getMessage(),null);
    }

    @Override
    public ResultVO get() {
        return this.resultVO;
    }
}
