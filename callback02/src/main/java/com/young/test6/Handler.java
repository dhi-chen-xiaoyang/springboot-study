package com.young.test6;

import com.young.vo.ResultVO;

public class Handler {
    private ResultVO result;
    private CallbackInterface callbackInterface;
    public void handler(int num,CallbackInterface callbackInterface){
        this.callbackInterface=callbackInterface;
        new Thread(()->{
            try{
                callbackInterface.checkParam(num);
                callbackInterface.process(num);
                Thread.sleep(3000);
            }catch (Exception e){
                e.printStackTrace();
                callbackInterface.fail(e);
                return;
            }
            callbackInterface.success();
        }).start();
    }

    public ResultVO get(){
        return callbackInterface.get();
    }
}
