package com.young.test6;

import com.young.vo.ResultVO;

public interface CallbackInterface {
    void checkParam(int num);
    void process(int num);
    void success();
    void fail(Exception e);
    ResultVO get();
}
