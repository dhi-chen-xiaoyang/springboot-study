package com.young.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultVO<T>implements Serializable {
    private Boolean flag;
    private String msg;
    private T data;
}
