package com.young.test2;

import com.young.vo.ResultVO;

public interface Callback {
    void checkParam(int num);
    void process(int num);
    ResultVO success();
    ResultVO fail(Exception e);
}
