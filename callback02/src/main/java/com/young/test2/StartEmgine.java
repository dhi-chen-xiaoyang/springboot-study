package com.young.test2;

import com.young.vo.ResultVO;

public class StartEmgine {
    public ResultVO handle(int num, Callback callback){
        try{
            callback.checkParam(num);
            callback.process(num);
        }catch (Exception e){
            e.printStackTrace();
            return callback.fail(e);
        }
        return callback.success();
    }
}
