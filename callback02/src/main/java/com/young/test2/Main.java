package com.young.test2;

import com.young.vo.ResultVO;

public class Main {
    public static void main(String[] args) {
        int num=0;
        StartEmgine startEmgine = new StartEmgine();
        ResultVO handle = startEmgine.handle(num, new Callback() {
            private int result = 0;

            @Override
            public void checkParam(int num) {
                if (num <= 0) {
                    throw new RuntimeException("参数有误");
                }
            }

            @Override
            public void process(int num) {
                result = num * 2;
            }

            @Override
            public ResultVO success() {
                return new ResultVO(true, "操作成功", result);
            }

            @Override
            public ResultVO fail(Exception e) {
                return new ResultVO(false, e.getMessage(), num);
            }
        });
        System.out.println(handle);
    }
}
