package com.young.test4;

import java.util.Random;
import java.util.concurrent.*;

public class MultiThreadCallback {
    public static void main(String[] args) throws ExecutionException ,InterruptedException {
        //这里简单地使用future和callable实现线程执行完后
        ExecutorService executor = Executors.newCachedThreadPool();
        Future<String> future = executor.submit(new Callable<String>() {

            @Override
            public String call() throws Exception {
                System.out.println("call");
                TimeUnit.SECONDS.sleep(1);
                return "str";
            }
        });
        //手动阻塞调用get通过call方法获得返回
        System.out.println(future.get());
        //需要手动关闭，不然线程池中的线程会继续执行
        executor.shutdown();
        //使用futuretask同时作为线程执行单元和数据请求单元
        FutureTask<Integer>futureTask=new FutureTask<>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println("dasds");
                return new Random().nextInt();
            }
        });
        new Thread(futureTask).start();
        //阻塞获取返回值
        System.out.println(futureTask.get());
    }
}
