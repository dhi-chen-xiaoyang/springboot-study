package com.young.test5;

import com.young.vo.ResultVO;

public class Test {
    public static void main(String[] args) {
        Handler handler=new Handler();
        ResultVO res = handler.handler(1, new CallbackInterface() {
            private Integer result = null;

            @Override
            public void checkParam(int num) {
                if (num % 2 == 0) {
                    throw new RuntimeException("参数有误");
                }
            }

            @Override
            public void process(int num) {
                result = num * 2 + 1;
            }

            @Override
            public ResultVO success() {
                return new ResultVO(true, "操作成功", result);
            }

            @Override
            public ResultVO fail(Exception e) {
                return new ResultVO(false, e.getMessage(), null);
            }
        });
        System.out.println(res);
        res = handler.handler(0, new CallbackInterface() {
            private Integer result = null;

            @Override
            public void checkParam(int num) {
                if (num % 2 == 0) {
                    throw new RuntimeException("参数有误");
                }
            }

            @Override
            public void process(int num) {
                result = num * 2 + 1;
            }

            @Override
            public ResultVO success() {
                return new ResultVO(true, "操作成功", result);
            }

            @Override
            public ResultVO fail(Exception e) {
                return new ResultVO(false, e.getMessage(), null);
            }
        });
        System.out.println(res);
    }
}
