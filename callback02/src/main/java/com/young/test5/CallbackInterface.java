package com.young.test5;

import com.young.vo.ResultVO;

public interface CallbackInterface {
    void checkParam(int num);
    void process(int num);
    ResultVO success();
    ResultVO fail(Exception e);
}
