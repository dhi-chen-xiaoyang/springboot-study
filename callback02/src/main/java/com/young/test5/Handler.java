package com.young.test5;

import com.young.vo.ResultVO;

public class Handler {
    public ResultVO handler(int num, CallbackInterface callbackInterface){
        try{
            callbackInterface.checkParam(num);
            callbackInterface.process(num);
        }catch (Exception e){
            e.printStackTrace();
            return callbackInterface.fail(e);
        }
        return callbackInterface.success();
    }
}
