# springboot-study

### 介绍
自己总结的SpringBoot学习资料，作为代码的备份，方便后续对一些知识点进行回顾，每一个项目都对应我在CSDN中的文章，
每更新一篇文章，都会将对应的文件夹上传到gitee中

### 软件架构
都是一些演示项目，因此架构基本都为单体架构，也有少量的dubbo项目和微服务项目

## 项目介绍
### 1. Trie01
Trie01是字典树的演示项目，在实际开发中，我们有时候对发布的文章、话题之类的内容，需要做一些审核，防止
一些含有不当词汇的文章发布到网络中，因此使用字典树对文章内容进行过滤。对应的文章连接为：
[使用TrieTree（字典树）来实现敏感词过滤](https://blog.csdn.net/weixin_55850954/article/details/130454886)

### 2. Annotation02
Annotation02是SpringBoot自定义注解的演示项目，通过自定义注解，实现鉴权、限流、参数校验等功能，
对应的文章连接为：[SpringBoot自定义注解](https://blog.csdn.net/weixin_55850954/article/details/130447246)

### 3. miaosha02
秒杀系统中通常会避免用户之间访问下单页面的URL(避免使用爬虫来造成不公平)。所有需要将URL动态化，即使秒杀系统的开发人员也无法在知晓在秒杀开始时的URL。
miaosha02 是对秒杀系统中，动态生成秒杀路径的介绍，参考文章：[秒杀系统中如何动态生成下单随机URL](https://blog.csdn.net/weixin_55850954/article/details/130450664),
[秒杀系统中如何动态生成下单随机URL](https://blog.csdn.net/canot/article/details/53966987)

### 4. threadPool01
threadPool01是关于线程池在SpringBoot一般如何使用的演示项目，包括一些线程池在使用的过程中可能出现的问题，
对应的文章连接为：[SpringBoot中线程池应用](https://blog.csdn.net/weixin_55850954/article/details/130308400)

### 5. threadPool03
threadPool03是在上一篇threadPool01的基础上，一个线程池应用的演示项目，参考了开源项目springboot-log,演示如何在项目中使用线程池批量插入日志，参考
文章连接：[SpringBoot中线程池应用](https://blog.csdn.net/weixin_55850954/article/details/130308400)

### 6. dubbo02
dubbo02是关于dubbo框架使用的演示项目，包括dubbo的负载均衡，调用机制，超时重试，本地存根等内容，对应的文章链接为：
[dubbo学习](https://blog.csdn.net/weixin_55850954/article/details/129764423)

### 7. dubbo03
dubbo03也是关于dubbo框架使用的演示项目，包括dubbo的灰度发布，服务鉴权等功能，对应的文章链接为：
[dubbo学习](https://blog.csdn.net/weixin_55850954/article/details/129764423)

### 8. chunk-file-demo
chunk-file-demo是关于大文件切片的演示项目，对应大文件的传输，可能会出现网络延迟等原因导致的次数失败，由此可以使用文件切片的方式。
对应的文章链接为：[SpringBoot文件切片](https://blog.csdn.net/weixin_55850954/article/details/129465162)

### 9. dictDemo02
dictDemo02是关于数据字典的演示项目，启动时将数据库中的数据字典加载到缓存中
对应的文章链接为：[SpringBoot使用数据字典](https://blog.csdn.net/weixin_55850954/article/details/130264642)

### 10. mail01
mail01是关于项目中如何整合邮件服务发送邮件信息的演示项目，通常用于一些需要用户验证或者提醒的场景，
对应的文章连接为：[SpringBoot发送邮箱验证码](https://blog.csdn.net/weixin_55850954/article/details/126883544)

### 11. springboot-admin01
springboot-admin01是关于项目中如何使用actuator来监测项目运行状况，包括http、jvm等情况，
对应的文章连接为：[SpringBoot-admin学习](https://blog.csdn.net/weixin_55850954/article/details/130478537)

### 12. redisson01
redisson01 是SpringBoot整合Redis实现分布式锁的演示项目，参考文章连接为：[Redis的分布式锁详解](https://blog.csdn.net/a745233700/article/details/88084219)

### 13. miaosha05
miaosha05是在B站学习网课时，记录的一些笔记，在秒杀系统中，我们把热门商品预热到redis中，然后在秒杀时扣减库存，并将订单消息添加
到消息队列中，从消息队列中插入订单信息。在这一系列过程中，我们应该考虑很多东西，如何将热门商品分布到不同节点？如果热门商品刚好分
布到同一个节点怎么办？如果这些热门商品中，有一个特别热门，几乎所有请求都打到该商品对应的节点上怎么办？如果redis集群挂了怎么办？
如何保证消息的幂等性？如何解决消息积压？对应的文章连接：[B站诸葛老师10万并发秒杀系统架构学习笔记](https://blog.csdn.net/weixin_55850954/article/details/130632097)

### 14. caffeine02
caffeine02是SpringBoot整合本地缓存caffeine的演示项目，参考文章连接为：[SpringBoot整合Caffeine](https://blog.csdn.net/weixin_55850954/article/details/130680519)

### 15. rpcException01
rpcException01是dubbo处理自定义异常的演示项目，参考文章连接为：[dubbo处理自定义异常](https://blog.csdn.net/weixin_55850954/article/details/130784276),[Dubbo如何处理业务异常，这个一定要知道哦](https://zhuanlan.zhihu.com/p/111947357)

### 16. python01
python01是关于java如何调用python脚本的演示项目，参考文章连接为：[Java 调用 Python 脚本](https://blog.csdn.net/IT__learning/article/details/119581363),
在python01的resource目录下，有参考的文档

### 17. Swagger03
swagger03是关于springboot整合swagger3(openApi3)的演示项目，同时还演示如何整合SpringSecurity和Swagger3，
参考文章连接[SpringBoot整合Swager3](https://blog.csdn.net/weixin_55850954/article/details/130820833)

### 18. readWrite01
readWrite01是关于springboot整合shardingsphere读写分离的演示项目，参考文章：[MySQL主从复制读写分离，看这篇就够了！](https://zhuanlan.zhihu.com/p/199217698)

### 19. cache01
cache01是关于caffeine本地缓存搭配redis实现二级缓存的演示项目，参考文章：https://www.yuque.com/g/chenhebufu/zspw94/fpebzheiabmk2pvr/collaborator/join?token=qs0jw5MKYHF7tCjU# 《二级缓存》

### 20. mapstruct01
mapstruct01是关于SpringBoot整合mapStruct的演示项目，MapStruct 是一个代码生成器，它基于约定优于配置方法极大地简化了 Java bean 类型之间映射的实现（通俗地说，就是做对象复制的）。
参考文章：https://www.yuque.com/g/chenhebufu/zspw94/nobsazpqaburbxze/collaborator/join?token=xv8bCXnsdD1DkuZx# 《MapStruct学习》

### 21. SpringEvent01
SpringEvent01是关于SpringBoot事件监听机制的演示项目，参考文章：https://www.yuque.com/g/chenhebufu/zspw94/agwbaawm53vqgguw/collaborator/join?token=XXiPIgGQ9fMfe0cV# 《Spring的事件机制》

### 22. flowable03 
flowable03是关于SpringBoot整合flowable6的演示项目，演示如何使用flowable6用户参与的流程活动，以及
不需要人工关于的业务流程，参考文章：https://www.yuque.com/g/chenhebufu/zspw94/my92sd5amn9b7ivy/collaborator/join?token=Xcv67F0UHSs8drrk# 《SpringBoot整合Flowable6》

### 23. callback02
callback02是关于Java使用回调机制的示例代码，主要代码在test4，test5，test6这三个包中，演示关于如何创建回调类，使用回调机制。
参考文章：https://www.yuque.com/g/chenhebufu/zspw94/fgey9erav5yhazg0/collaborator/join?token=O6To8tH0EQ6uiwVS# 《Java使用回调》

### 24. mybatis-plus01
mybatis-plus01是关于在SpringBoot整合mybatisPlus中出现的问题的一些总结，如果使用mybatisPlus，为了避免mybatisPlus中的诸如LambdaQuery等内容在service中的耦合，我们将mybatisPlus中
的相关代码封装到repository中，也就是说，service调用repository，repository中编写mybatisPlus的相关逻辑然后调用dao层，从而避免mybatisPlus在service层中的高侵入性。
参考文章：https://www.yuque.com/g/chenhebufu/zspw94/pxe6udxgaw49a510/collaborator/join?token=KID0qQRnGtGYdwvs# 《关于Mybatis-plus使用的一些细节》

### 25. strategyAndFactory01
strategyAndFactory01是关于在SpringBoot使用策略模式结合工厂模式进行开发的一些总结，相关文章连接：https://www.yuque.com/g/chenhebufu/zspw94/uqfr477xawg4bq5w/collaborator/join?token=UFG6ZXT0gvLh3s5d# 《SpringBoot中使用策略模式加工厂模式》

### 26. ddd01
ddd01是关于如何使用DDD领域驱动设计来创建项目的示例代码，参考文章：https://www.yuque.com/g/chenhebufu/zspw94/ogl2159lyozkbir2/collaborator/join?token=F0Up84GBh9rwRN5k# 《DDD领域驱动设计实战一》

### 测试一下哈哈哈哈哈哈
我的天哪

## 开源项目学习
### 1. hdw-dubbo
hdw-dubbo微服务开发平台，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理等多个模块，支持多业务系统并行开发，可以作为后端服务的开发脚手架。代码简洁，架构清晰，适合学习和直接项目中使用。
项目的gitee地址为：
https://gitee.com/tumao2/hdw-dubbo?_from=gitee_search

### 2. springboot-log
springboot-log项目使用线程池，实现接口日志异步池化入库： 将接口中的日志内容直接放入到队列中，不需要进行日志入库了，减少了一次数据库操作，可以提升接口的响应速度 。
库中日志多线程清理： 当日志表中的数据量过多时，会占用很多的磁盘空间，进而可能导致磁盘空间不足的告警，此时就需要进行日志的清理，但是此时在清理时，还需要将最新多少天的数据保留，将之前的日志数据清理掉 ；日志清理是 定时 进行清理的，一般在凌晨访问量最少的时候进行日志的清理。
github链接：https://github.com/leishen6/springboot_log

### 3. leave-sample
leave-sample项目是关于DDD领域驱动设计的演示项目，中台架构与实现 DDD和微服务，清晰地提供了从战略设计到战术设计以及代码落地。
gitee地址：https://gitee.com/serpmelon/leave-sample

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  本项目仅作为代码的备份，方便后续对一些知识点进行回顾
2.  每一个项目都对应我在CSDN中的文章

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
