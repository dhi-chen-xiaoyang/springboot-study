package com.young.service.impl;

import com.young.entity.User;
import com.young.mapper.UserMapper;
import com.young.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public Boolean saveUser(User user) {
        log.info("saveUser==========");
        return userMapper.insert(user)>0;
    }

    @Override
    public User getUserById(Integer id) {
        log.info("getUserById==========");
        return userMapper.selectById(id);
    }
}
