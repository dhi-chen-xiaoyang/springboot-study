package com.young.service;

import com.young.entity.User;

public interface UserService {
    Boolean saveUser(User user);
    User getUserById(Integer id);
}
