package com.young;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.young.mapper")
public class ReadWrite01Application {
    public static void main(String[] args) {
        SpringApplication.run(ReadWrite01Application.class,args);
    }
}
