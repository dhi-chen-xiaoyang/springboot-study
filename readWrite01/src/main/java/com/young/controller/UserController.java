package com.young.controller;

import com.young.entity.User;
import com.young.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") Integer id){
        User user = userService.getUserById(id);
        System.out.println(user);
        return user;
    }

    @PostMapping
    public String saveUser(@RequestBody User user){
        Boolean flag = userService.saveUser(user);
        if (!flag){
            return "fail";
        }
        return "success";
    }
}
