package com.young.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.young.config.MasterProp;
import com.young.config.Slave0Prop;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.config.masterslave.LoadBalanceStrategyConfiguration;
import org.apache.shardingsphere.spring.boot.util.DataSourceUtil;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Configuration
//@EnableTransactionManagement(proxyTargetClass = true)
//@EnableConfigurationProperties({MasterProp.class, Slave0Prop.class})
//@Slf4j
//@MapperScan(basePackages = "com.young.mapper",sqlSessionTemplateRef = "sqlSessionTemplate")
//public class DataSourceConfig {
//    //配置数据源0
//    @Bean(name = "master")
//    public DataSource master(MasterProp masterProp) throws ReflectiveOperationException {
//        Map<String,Object>dsMap=new HashMap<>();
//        dsMap.put("type",masterProp.getType());
//        dsMap.put("url",masterProp.getUrl());
//        dsMap.put("username",masterProp.getUsername());
//        dsMap.put("password",masterProp.getPassword());
//        dsMap.put("driverClassName",masterProp.getDriverClassName());
//        DruidDataSource ds=(DruidDataSource) DataSourceUtil.getDataSource(masterProp.getDriverClassName(),dsMap);
//        //每个分区最大连接数
//        ds.setMaxActive(20);
//        //每个分区最小连接数
//        ds.setMinIdle(5);
//        return ds;
//    }
//
//    //配置数据源1
//    @Bean(name = "slave0")
//    public DataSource slave0(Slave0Prop slave0Prop) throws ReflectiveOperationException {
//        Map<String,Object>dsMap=new HashMap<>();
//        dsMap.put("type",slave0Prop.getType());
//        dsMap.put("url",slave0Prop.getUrl());
//        dsMap.put("username",slave0Prop.getUsername());
//        dsMap.put("password",slave0Prop.getPassword());
//        dsMap.put("driverClassName",slave0Prop.getDriverClassName());
//        DruidDataSource ds=(DruidDataSource) DataSourceUtil.getDataSource(slave0Prop.getDriverClassName(),dsMap);
//        //每个分区最大连接数
//        ds.setMaxActive(20);
//        //每个分区最小连接数
//        ds.setMinIdle(5);
//        return ds;
//    }
//
//    @Bean("dataSource")
//    public DataSource dataSource(@Qualifier("master")DataSource master,
//                                 @Qualifier("slave0")DataSource slave0)throws SQLException{
//        //配置真实数据源
//        Map<String,DataSource>dataSourceMap=new HashMap<>();
//        dataSourceMap.put("master",master);
//        dataSourceMap.put("slave0",slave0);
//
//        List<String>slaveList=new ArrayList<>();
//        slaveList.add("slave0");
//
//        //从库负载策略
//        LoadBalanceStrategyConfiguration loadBalanceStrategyConfiguration=new LoadBalanceStrategyConfiguration("round_robin");
//
//    }
//
//    @Bean("sqlSessionTemplate")
//    @Primary
//    public SqlSessionTemplate sqlSessionTemplate()
//}
