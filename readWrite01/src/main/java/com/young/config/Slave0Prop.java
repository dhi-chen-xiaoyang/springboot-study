package com.young.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "spring.shardingsphere.datasource.slave0")
@Configuration
@Data
public class Slave0Prop {
    private String url;
    private String username;
    private String password;
    private String type;
    private String driverClassName;
}
