package com.young.controller;

import com.young.model.Exposer;
import com.young.service.SeckillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/miaosha")
public class TestController {
    @Autowired
    private SeckillService seckillService;
    @RequestMapping(value = "/{goodsId}/getUrl")
    public Exposer exposer(@PathVariable("goodsId")Long seckillGoodsId){
        Exposer result=seckillService.exportSeckillUrl(seckillGoodsId);
        return result;
    }

    @RequestMapping(value = "/{seckillGoodsId}/{md5}/execution")
    public Boolean execution(@PathVariable("seckillGoodsId")Long seckillGoodsId,
                             @PathVariable("md5")String md5){
        Boolean result=seckillService.executionSeckillId(seckillGoodsId,md5);
        return result;
    }
}
