package com.young.service;

import com.young.model.Exposer;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

@Service
public class SeckillService {
    private final String salt="12sadasadsafafafs。/。，";
    public Exposer exportSeckillUrl(Long seckillGoodsId) {
        String md5=getMD5(seckillGoodsId);
        return new Exposer(md5);
    }
    private String getMD5(Long seckillGoodsId){
        String base=seckillGoodsId+"/"+salt;
        String md5= DigestUtils.md5DigestAsHex(base.getBytes());
        return md5;
    }

    public Boolean executionSeckillId(Long seckillGoodsId, String md5) {
        if (md5 == null || !md5.equals(getMD5(seckillGoodsId))) {
            return false;
        }
        return true;
    }
}
