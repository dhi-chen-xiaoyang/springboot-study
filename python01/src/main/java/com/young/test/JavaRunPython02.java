package com.young.test;

import org.python.util.PythonInterpreter;

public class JavaRunPython02 {
    public static void main(String[] args) {
        String python="D:\\python\\simple_python.py";
        PythonInterpreter interp=new PythonInterpreter();
        interp.execfile(python);
        interp.cleanup();
        interp.close();
    }
}
