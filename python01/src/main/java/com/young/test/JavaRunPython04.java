package com.young.test;

import org.python.antlr.ast.Str;
import org.python.bouncycastle.jcajce.provider.asymmetric.rsa.RSAUtil;

import java.io.*;
import java.util.concurrent.ExecutionException;

public class JavaRunPython04 {
    public static void main(String[] args) {
//        Process proc;
//        try{
//            proc=Runtime.getRuntime().exec("python D:\\python\\demo1.py");
//            //使用输入输出流来截取结果
//            BufferedReader in=new BufferedReader(new InputStreamReader(proc.getInputStream()));
//            String line=null;
//            System.out.println(in.readLine());
//            while ((line=in.readLine())!=null){
//                System.out.println(line);
//            }
//            in.close();
//            proc.waitFor();
//        }catch (IOException e){
//            e.printStackTrace();
//        }catch (InterruptedException e){
//            e.printStackTrace();
//        }
//        Process proc;
//        BufferedInputStream bufferedInputStream=null;
//        try{
//            String compile="C:\\Python27\\python.exe";
//            proc=Runtime.getRuntime().exec(compile+" D:\\python\\demo1.py");
//            //使用输入输出流截取结果
//            bufferedInputStream = new BufferedInputStream(proc.getInputStream());
//            byte[]buffer=new byte[2048];
//            int len=0;
//            while ((len=bufferedInputStream.read(buffer))!=-1){
//                String line=new String(buffer,0,len);
//                System.out.println(line);
//            }
//            bufferedInputStream=new BufferedInputStream(proc.getErrorStream());
//            while ((len=bufferedInputStream.read(buffer))!=-1){
//                String line=new String(buffer,0,len);
//                System.out.println(line);
//            }
//            proc.waitFor();
//        }catch (IOException e){
//            e.printStackTrace();
//        }catch (InterruptedException e){
//            e.printStackTrace();
//        }
        Process proc;
        try{
            //compile和运行的pythonFile一定要写绝对路径
            String pythonFile="D:\\python\\demo1.py";
            String compile="C:\\Users\\DHY\\AppData\\Local\\Programs\\Python\\Python39\\python.exe";
            String[]argg=new String[]{compile,pythonFile};
            proc=Runtime.getRuntime().exec(argg);
            BufferedReader in=new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line=null;
            while((line=in.readLine())!=null){
                System.out.println(line);
            }
            in.close();
            proc.waitFor();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
