package com.young.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class JavaRunPython05 {
    public static void main(String[] args) {
        int a=18;
        int b=23;
        try{
            String compile="C:\\Users\\DHY\\AppData\\Local\\Programs\\Python\\Python39\\python.exe";
            String pythonFile="D:\\python\\func.py";
            String[]argg=new String[]{compile,pythonFile,String.valueOf(a),String.valueOf(b)};
            Process proc=Runtime.getRuntime().exec(argg);
            BufferedReader in=new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line=null;
            while((line=in.readLine())!=null){
                System.out.println(line);
            }
            in.close();
            proc.waitFor();
        }catch (IOException e){
            e.printStackTrace();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
