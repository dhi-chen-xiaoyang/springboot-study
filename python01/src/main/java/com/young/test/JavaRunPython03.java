package com.young.test;

import org.python.core.PyFunction;
import org.python.core.PyInteger;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

public class JavaRunPython03 {
    public static void main(String[] args) {
        String python="D:\\python\\add.py";
        PythonInterpreter interp=new PythonInterpreter();
        interp.execfile(python);
        //第一个参数为要执行的函数名，第二个参数为期望返回的对象类型
        PyFunction pyFunction=interp.get("add",PyFunction.class);
        int a=5,b=10;
        //调用函数，如果函数需要参数，在java中必须将参数转化为对应的python类型
        PyObject pyObject = pyFunction.__call__(new PyInteger(a), new PyInteger(b));
        System.out.println("the answer is :"+pyObject);
        interp.cleanup();
        interp.close();
    }
}
