package com.young.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class PythonUtil {
    @Value("${python.compile}")
    private String compile;

    /**
     * 调用没有参数的脚本，返回InputStream
     * @param pythonFile
     * @return
     */
    public InputStream runPython(String pythonFile){
        Process proc;
        try{
            String[]args=new String[]{compile,pythonFile};
            proc=Runtime.getRuntime().exec(args);
            return proc.getInputStream();
        }catch (Exception e) {
            log.info(e.getMessage());
            return null;
        }
    }

    /**
     * 调用没有参数的脚本，以String返回
     * @param pythonFile
     * @return
     */
    public String runPythonStr(String pythonFile){
        Process proc;
        try{
            String[]args=new String[]{compile,pythonFile};
            proc=Runtime.getRuntime().exec(args);
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(proc.getInputStream()));
            StringBuffer stringBuffer=new StringBuffer();
            String line=null;
            while ((line=bufferedReader.readLine())!=null){
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            return stringBuffer.toString();
        }catch (Exception e){
            log.info(e.getMessage());
            return null;
        }
    }

    /**
     * 调用没有参数的脚本，返回InputStream
     * @param pythonFile
     * @return
     */
    public InputStream runPythonWithArgs(String pythonFile,String... argg){
        Process proc;
        try{
            int length = argg.length;
            List<String>list=new ArrayList<>();
            list.add(compile);
            list.add(pythonFile);
            for(int i=0;i<length;i++){
                list.add(argg[i]);
            }
            //将list作为String数组
            String[]args=list.toArray(new String[list.size()]);
            proc=Runtime.getRuntime().exec(args);
            return proc.getInputStream();
        }catch (Exception e) {
            log.info(e.getMessage());
            return null;
        }
    }

    /**
     * 调用没有参数的脚本，以String返回
     * @param pythonFile
     * @return
     */
    public String runPythonStrWithArgs(String pythonFile,String... argg){
        Process proc;
        try{
            int length = argg.length;
            List<String>list=new ArrayList<>();
            list.add(compile);
            list.add(pythonFile);
            for(int i=0;i<length;i++){
                list.add(argg[i]);
            }
            //将list作为String数组
            String[]args=list.toArray(new String[list.size()]);
            proc=Runtime.getRuntime().exec(args);
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(proc.getInputStream()));
            StringBuffer stringBuffer=new StringBuffer();
            String line=null;
            while ((line=bufferedReader.readLine())!=null){
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            return stringBuffer.toString();
        }catch (Exception e){
            log.info(e.getMessage());
            return null;
        }
    }
}
