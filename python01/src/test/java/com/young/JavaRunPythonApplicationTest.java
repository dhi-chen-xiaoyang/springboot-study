package com.young;

import com.young.util.PythonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class JavaRunPythonApplicationTest {
    @Resource
    private PythonUtil pythonUtil;

    @Test
    public void test1(){
        String pythonFile="D:\\python\\demo1.py";
        String res = pythonUtil.runPythonStr(pythonFile);
        System.out.println(res);
    }

    @Test
    public void test2(){
        String pythonFile="D:\\python\\func.py";
        String res = pythonUtil.runPythonStrWithArgs(pythonFile, "18", "23");
        System.out.println(res);
    }
}
