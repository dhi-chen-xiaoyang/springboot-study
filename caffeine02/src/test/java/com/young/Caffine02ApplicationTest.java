package com.young;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.young.entity.User;
import com.young.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Result;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SpringBootTest
@Slf4j
public class Caffine02ApplicationTest {
    @Autowired
    private UserService userService;

    @Resource
    @Qualifier(value = "manualCaffeineCache")
    private Cache<String,String> manualCaffineCache;

    @Test
    public void testCache(){
        //获取缓存
        User user = userService.getUserById(1);
        log.info("第一次从数据库获取缓存：{}",user);
        user=userService.getUserById(1);
        log.info("第二次从缓存中获取：{}",user);
        //过期时间为10秒，我们10秒后再获取
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        user=userService.getUserById(1);
        log.info("10秒后再次获取user：{}",user);
    }

    @Test
    public void testManualCaffeineCache(){
        //将数据放入缓存
        manualCaffineCache.put("The best language","java");
        //获取key对应的value，如果不存在，返回null
        String the_best_language = manualCaffineCache.getIfPresent("The best language");
        System.out.println(the_best_language);
        //删除entry
        manualCaffineCache.invalidate("The best language");
        the_best_language= manualCaffineCache.getIfPresent("The best language");
        System.out.println(the_best_language);

        //以map的形式进行增删改查==================
        manualCaffineCache.asMap().put("best","java");
        manualCaffineCache.asMap().put("best1","SpringBoot");
        String best = manualCaffineCache.asMap().get("best");
        String best1 = manualCaffineCache.asMap().get("best1");
        System.out.println("best:"+best);
        System.out.println("best1:"+best1);
        //删除entry
        manualCaffineCache.asMap().remove("best");
        manualCaffineCache.asMap().remove("best1");
        best = manualCaffineCache.asMap().get("best");
        best1 = manualCaffineCache.asMap().get("best1");
        System.out.println("best:"+best);
        System.out.println("best1:"+best1);
    }

    @Resource
    @Qualifier(value = "loadingCaffeineCache")
    private LoadingCache<String,Object> loadingCaffeineCache;

    @Test
    public void testLoadingCaffeineCache(){
        User user=new User();
        user.setId(1);
        user.setUsername("cxy");
        user.setPassword("123456");
        user.setAge(20);
        user.setSex("男");
        loadingCaffeineCache.put("1",user);
        User res=(User)loadingCaffeineCache.getIfPresent("1");
        System.out.println("res:"+res);
        res=(User)loadingCaffeineCache.getIfPresent("2");
        System.out.println("res:"+res);
        List<String>list= Arrays.asList("1","2","3");
        Map<@NonNull String, @NonNull Object> resMap = loadingCaffeineCache.getAllPresent(list);
        System.out.println("resMap:"+resMap);
        System.out.println("上面调用的都是IfPresent(),即存在才返回，因此不会触发我们刚才的两个load函数==========");
        res=(User)loadingCaffeineCache.get("2");
        System.out.println("res:"+res);
        resMap= loadingCaffeineCache.getAll(list);
        System.out.println("resMap:"+resMap);
    }

    @Resource
    @Qualifier(value = "listenerCaffeineCache")
    private Cache<String,Object>listenerCaffeineCache;

    @Test
    public void testListenerCaffeineCache() throws InterruptedException {
        listenerCaffeineCache.put("cxy","程序员");
        listenerCaffeineCache.put("best","java");
        listenerCaffeineCache.invalidate("cxy");
        listenerCaffeineCache.asMap().remove("best");
    }
}
