package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Caffeine02Application {
    public static void main(String[] args) {
        SpringApplication.run(Caffeine02Application.class,args);
    }
}
