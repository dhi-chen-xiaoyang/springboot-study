package com.young.service;

import com.young.entity.User;

public interface UserService {
    Boolean saveUser(User user);
    Boolean updateUser(User user);
    Boolean deleteUserById(Integer id);
    User getUserById(Integer id);
}
