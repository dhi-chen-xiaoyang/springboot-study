package com.young.client.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/demo")
public class DemoController {
    @GetMapping
    public String getDemo(){
        return "Hello SpringBoot-admin";
    }
    @GetMapping("/{id}")
    public String getDemoById(@PathVariable("id")String id){
        return "Hello Spring-boot admin"+id;
    }
}
