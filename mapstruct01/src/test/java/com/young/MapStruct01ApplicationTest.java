package com.young;

import com.young.convertor.MainConvertor;
import com.young.dto.DeliveryAddressDTO;
import com.young.dto.StudentDTO;
import com.young.dto.UserDTO;
import com.young.pojo.Address;
import com.young.pojo.Doctor;
import com.young.vo.StudentVO;
import com.young.vo.UserVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class MapStruct01ApplicationTest {
    @Autowired
    private MainConvertor mainConvertor;

    @Test
    public void testSimpleMap(){
        StudentVO studentVO=StudentVO.builder()
                .school("清华大学")
                .userId("cxy")
                .userName("程序员")
                .age(21)
                .address("杭州")
                .emailAddress("123@qq.com")
                .build();
        StudentDTO studentDTO = mainConvertor.studentVO2DTO(studentVO);
        System.out.println(studentDTO);
    }

    @Test
    public void testStudentVOList2DTO(){
        List<StudentVO>studentVOList=new ArrayList<>();
        for(int i=0;i<5;i++){
            StudentVO studentVO=StudentVO.builder()
                    .school("清华大学"+i)
                    .userId("cxy"+i)
                    .userName("程序员"+i)
                    .age(21+i)
                    .address("杭州"+i)
                    .emailAddress("123@qq.com"+i)
                    .build();
            studentVOList.add(studentVO);
        }
        List<StudentDTO> studentDTOS = mainConvertor.studentListVO2DTO(studentVOList);
        System.out.println(studentDTOS);
    }

    @Test
    public void testStudentDTOList2VO(){
        List<StudentDTO>studentDTOList=new ArrayList<>();
        for(int i=0;i<5;i++){
            StudentDTO studentDTO=StudentDTO.builder()
                    .school("清华大学"+i)
                    .userId("cxy"+i)
                    .userName("程序员"+i)
                    .age(21+i)
                    .address("杭州"+i)
                    .email("123@qq.com"+i)
                    .build();
            studentDTOList.add(studentDTO);
        }
        List<StudentVO> studentVOS = mainConvertor.studentListDTO2VO(studentDTOList);
        System.out.println(studentVOS);
    }

    @Test
    public void testUserVO2DTO(){
        UserVO userVO=UserVO.builder()
                .id(1)
                .username("程序员")
                .password("123456")
                .isDeleted(false)
                .build();
        UserDTO userDTO = mainConvertor.userVO2DTO(userVO);
        System.out.println(userDTO);
    }

    @Test
    public void testDoctorAndAddress2DeliveryAddressDTO(){
        Doctor doctor=new Doctor();
        doctor.setId(1);
        doctor.setName("程序员");
        doctor.setIsDelete(0);
        Address address=new Address();
        address.setStreet("华南理工大学");
        address.setZipCode(123456);
        address.setIsDelete(1);
        DeliveryAddressDTO deliveryAddressDTO = mainConvertor.doctorAndAddress2DeliveryAddressDTO(doctor, address);
        System.out.println(deliveryAddressDTO);
    }
}
