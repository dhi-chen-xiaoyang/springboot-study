package com.young.convertor;

import com.young.dto.DeliveryAddressDTO;
import com.young.dto.StudentDTO;
import com.young.dto.UserDTO;
import com.young.pojo.Address;
import com.young.pojo.Doctor;
import com.young.vo.StudentVO;
import com.young.vo.UserVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MainConvertor {
    @Mapping(source = "emailAddress",target = "email")
    StudentDTO studentVO2DTO(StudentVO studentVO);

    List<StudentDTO>studentListVO2DTO(List<StudentVO>studentVOList);

    @Mapping(source = "email",target = "emailAddress")
    StudentVO studentDTO2VO(StudentDTO studentDTO);

    List<StudentVO>studentListDTO2VO(List<StudentDTO>studentDTOList);

    default UserDTO userVO2DTO(UserVO userVO){
        if (userVO==null){
            return null;
        }
        UserDTO userDTO=UserDTO.builder()
                .id(userVO.getId())
                .username(userVO.getUsername())
                .password(userVO.getPassword())
                .build();
        if (userVO.getIsDeleted()){
            userDTO.setIsDeleted(1);
        }else{
            userDTO.setIsDeleted(0);
        }
        return userDTO;
    }

    @Mapping(source = "doctor.id",target = "personId")
    @Mapping(source = "address.street",target = "streetDesc")
    @Mapping(source = "doctor.isDelete",target = "isDelete")
    public DeliveryAddressDTO doctorAndAddress2DeliveryAddressDTO(Doctor doctor, Address address);
}
