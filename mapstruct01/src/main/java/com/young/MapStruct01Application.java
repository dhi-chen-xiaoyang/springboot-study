package com.young;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapStruct01Application {
    public static void main(String[] args) {
        SpringApplication.run(MapStruct01Application.class,args);
    }
}
