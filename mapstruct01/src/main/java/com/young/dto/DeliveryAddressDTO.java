package com.young.dto;

import lombok.Data;

@Data
public class DeliveryAddressDTO {
    private Integer personId;
    private String name;
    private String streetDesc;
    private Integer zipCode;
    private Integer isDelete;
}
