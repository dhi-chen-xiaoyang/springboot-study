package com.young.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
@Data
@Builder
public class StudentDTO implements Serializable {
    private String userName;
    private String userId;
    private String address;
    private String school;
    private int age;
    private String email;
}
