package com.young.pojo;

import lombok.Data;

@Data
public class Doctor {
    private Integer id;
    private String name;
    private Integer isDelete;
}
