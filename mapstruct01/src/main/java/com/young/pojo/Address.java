package com.young.pojo;

import lombok.Data;

@Data
public class Address {
    private String street;
    private Integer zipCode;
    private Integer isDelete;
}
