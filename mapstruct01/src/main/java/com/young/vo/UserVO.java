package com.young.vo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserVO {
    private Integer id;
    private String username;
    private String password;
    private Boolean isDeleted;
}
