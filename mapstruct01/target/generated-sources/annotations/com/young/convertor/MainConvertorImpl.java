package com.young.convertor;

import com.young.dto.DeliveryAddressDTO;
import com.young.dto.StudentDTO;
import com.young.dto.StudentDTO.StudentDTOBuilder;
import com.young.pojo.Address;
import com.young.pojo.Doctor;
import com.young.vo.StudentVO;
import com.young.vo.StudentVO.StudentVOBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-17T19:44:04+0800",
    comments = "version: 1.4.1.Final, compiler: javac, environment: Java 1.8.0_301 (Oracle Corporation)"
)
@Component
public class MainConvertorImpl implements MainConvertor {

    @Override
    public StudentDTO studentVO2DTO(StudentVO studentVO) {
        if ( studentVO == null ) {
            return null;
        }

        StudentDTOBuilder studentDTO = StudentDTO.builder();

        studentDTO.email( studentVO.getEmailAddress() );
        studentDTO.userName( studentVO.getUserName() );
        studentDTO.userId( studentVO.getUserId() );
        studentDTO.address( studentVO.getAddress() );
        studentDTO.school( studentVO.getSchool() );
        studentDTO.age( studentVO.getAge() );

        return studentDTO.build();
    }

    @Override
    public List<StudentDTO> studentListVO2DTO(List<StudentVO> studentVOList) {
        if ( studentVOList == null ) {
            return null;
        }

        List<StudentDTO> list = new ArrayList<StudentDTO>( studentVOList.size() );
        for ( StudentVO studentVO : studentVOList ) {
            list.add( studentVO2DTO( studentVO ) );
        }

        return list;
    }

    @Override
    public StudentVO studentDTO2VO(StudentDTO studentDTO) {
        if ( studentDTO == null ) {
            return null;
        }

        StudentVOBuilder studentVO = StudentVO.builder();

        studentVO.emailAddress( studentDTO.getEmail() );
        studentVO.userName( studentDTO.getUserName() );
        studentVO.userId( studentDTO.getUserId() );
        studentVO.address( studentDTO.getAddress() );
        studentVO.school( studentDTO.getSchool() );
        studentVO.age( studentDTO.getAge() );

        return studentVO.build();
    }

    @Override
    public List<StudentVO> studentListDTO2VO(List<StudentDTO> studentDTOList) {
        if ( studentDTOList == null ) {
            return null;
        }

        List<StudentVO> list = new ArrayList<StudentVO>( studentDTOList.size() );
        for ( StudentDTO studentDTO : studentDTOList ) {
            list.add( studentDTO2VO( studentDTO ) );
        }

        return list;
    }

    @Override
    public DeliveryAddressDTO doctorAndAddress2DeliveryAddressDTO(Doctor doctor, Address address) {
        if ( doctor == null && address == null ) {
            return null;
        }

        DeliveryAddressDTO deliveryAddressDTO = new DeliveryAddressDTO();

        if ( doctor != null ) {
            deliveryAddressDTO.setPersonId( doctor.getId() );
            deliveryAddressDTO.setIsDelete( doctor.getIsDelete() );
            deliveryAddressDTO.setName( doctor.getName() );
        }
        if ( address != null ) {
            deliveryAddressDTO.setStreetDesc( address.getStreet() );
            deliveryAddressDTO.setZipCode( address.getZipCode() );
        }

        return deliveryAddressDTO;
    }
}
